﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene_shred_script : MonoBehaviour
{

    public static Scene_shred_script Instance;


    public Sprite temp_profile_pic;
    public int reward, fee;
    public string U_name;


    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }


        Instance = this;
        DontDestroyOnLoad(this.gameObject);


    }


    public void OnStart_lvl()
    {
        SceneManager.LoadScene("Gameplay");

        //set_gameplay_panel();
    }

    public void set_gameplay_panel()
    {
        Gamplay_panel_script.I.player_profile_pic[3].sprite = temp_profile_pic;
        Gamplay_panel_script.I.Usernames[3].text = U_name;
    }

    // Start is called before the first frame update
    void Start()
    {

    }


    public void start_game_again()
    {
        Time.timeScale = 1;
        Splash_Panel_scripts.I.custom_start();
    }
    // Update is called once per frame
    void Update()
    {

    }

    public void Upload_data_server_done()
    {
        SceneManager.LoadScene("MainMenu");
        GameManager.Instance.start_game_again();
    }



    public void Uploaddata_server()
    {
        Update_API.Instance.is_update = 1;
        Update_API.Instance.type = "gamePlay";
        Update_API.Instance.credits = PlayerPrefs.GetInt("Coin_value", 0);
        Update_API.Instance.xp = PlayerPrefs.GetInt("Level_value", 0);
        Update_API.Instance.Update_data();
    }
}

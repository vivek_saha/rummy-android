﻿using rummy.Cards;
using rummy.Utility;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace rummy
{
    public class GameMaster : MonoBehaviour
    {
        public GameObject WinPanel, LosePanel;
        public bool gamestarted = false;
        public int Seed;

        public float GameSpeed = 1.0f;

        public void SetGameSpeed(float value) => GameSpeed = value;

        public bool AnimateCardMovement = true;

        public void SetAnimateCardMovement(bool value) => AnimateCardMovement = value;

        private float drawWaitStartTime;
        public float DrawWaitDuration = 2f;

        public void SetDrawWaitDuration(float value) => DrawWaitDuration = value;

        public float PlayWaitDuration = 2f;

        public void SetPlayWaitDuration(float value) => PlayWaitDuration = value;

        public int CardsPerPlayer = 13;
        public int EarliestAllowedLaydownRound = 2;
        public int MinimumLaySum = 40;
        public float CardMoveSpeed = 50f;

        public int RoundCount { get; private set; }

        /// <summary> Returns whether laying down cards in the current round is allowed </summary>
        public bool LayingAllowed() => RoundCount >= EarliestAllowedLaydownRound;

        private List<Player> Players = new List<Player>();
        public Player CurrentPlayer { get { return Players[currentPlayerID]; } }

        private bool isCardBeingDealt;
        private int currentPlayerID;
        private bool skippingDone;

        /// <summary>
        /// FOR DEV PURPOSES ONLY! Disable card movement animation and set the wait durations to 0 until the given round starts. '0' means no round is skipped
        /// </summary>
        public int SkipUntilRound = 0;

        private float tmpPlayerWaitDuration, tmpDrawWaitDuration, DefaultGameSpeed;

        private enum GameState
        {
            NONE = 0,
            DEALING = 1,
            PLAYING = 2,
            DRAWWAIT = 3
        }

        private GameState gameState = GameState.NONE;

        public class Event_GameOver : UnityEvent<Player> { }

        public Event_GameOver GameOver = new Event_GameOver();

        [SerializeField]
        private CardStack.CardStackType CardStackType = CardStack.CardStackType.DEFAULT;

        private void Start()
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 60;

            Players = FindObjectsOfType<Player>().OrderBy(p => p.name[p.name.Length - 1]).ToList();

            //random seed for random cards
            Seed = Random.Range(1, 20000);
            Random.InitState(Seed);
            Tb.I.CardStack.CreateCardStack(CardStackType);
            StartGame();
        }

        private void StartGame()
        {
            DefaultGameSpeed = GameSpeed;
            tmpPlayerWaitDuration = PlayWaitDuration;
            tmpDrawWaitDuration = DrawWaitDuration;
            if (SkipUntilRound > 0)
            {
                skippingDone = false;
                AnimateCardMovement = false;
                PlayWaitDuration = 0f;
                DrawWaitDuration = 0f;
                GameSpeed = 4;
            }

            Time.timeScale = GameSpeed;
            RoundCount = 0;

            gameState = GameState.DEALING;
            currentPlayerID = 0;
        }

        public void NextGame()
        {
            Seed += 1;
            RestartGame();
        }

        public void RestartGame()
        {
            if (SkipUntilRound > 0)
            {
                GameSpeed = DefaultGameSpeed;
                DrawWaitDuration = tmpDrawWaitDuration;
                PlayWaitDuration = tmpPlayerWaitDuration;
            }

            var cards = new List<Card>();
            cards.AddRange(Tb.I.DiscardStack.RemoveCards());
            foreach (var p in Players)
                cards.AddRange(p.ResetPlayer());

            Random.InitState(Seed);
            Tb.I.CardStack.Restock(cards, true);
            StartGame();
        }

        private void Update()
        {
            Time.timeScale = GameSpeed;

            if (gameState == GameState.DEALING)
            {
                if (!isCardBeingDealt)
                {
                    isCardBeingDealt = true;
                    CurrentPlayer.DrawCard(true);
                }
                else if (CurrentPlayer.State == Player.PlayerState.IDLE)
                {
                    isCardBeingDealt = false;
                    //Debug.Log(currentPlayerID);
                    currentPlayerID = (currentPlayerID + 1) % Players.Count;

                    if (currentPlayerID == 0 && CurrentPlayer.HandCardCount == CardsPerPlayer)
                    {
                        gameState = GameState.PLAYING;
                        RoundCount = 1;
                        TryStopSkipping();
                    }
                }
            }
            else if (gameState == GameState.PLAYING)
            {
                
                if (CurrentPlayer.State == Player.PlayerState.IDLE)
                {
                    if(CurrentPlayer.player_type_ == Player_Type.User)
                    {
                        Gamplay_panel_script.I.Enable_player_buttons();
                    }
                    else
                    {
                        Gamplay_panel_script.I.Disable_player_buttons();
                    }
                    Gamplay_panel_script.I.Change_Active_player(currentPlayerID);
                    //Debug.Log(CurrentPlayer);
                    //Debug.Log(currentPlayerID);
                    CurrentPlayer.TurnFinished.AddListener(PlayerFinished);
                    CurrentPlayer.BeginTurn();
                }
            }
            else if (gameState == GameState.DRAWWAIT)
            {
                if (Time.time - drawWaitStartTime > DrawWaitDuration)
                    gameState = GameState.PLAYING;
            }
        }

        public void PlayerFinished()
        {
            UndoRedoSystem.I.Reset_Undo();
            CurrentPlayer.TurnFinished.RemoveAllListeners();
            if (CurrentPlayer.HandCardCount == 0)
            {
                if (CurrentPlayer.player_type_ == Player_Type.User)
                {
                    WinPanel.SetActive(true);
                    WinPanel.GetComponent<WInPanel_script>().u_name.text = Scene_shred_script.Instance.U_name;
                    WinPanel.GetComponent<WInPanel_script>().Win_coin_value.text = "+"+Scene_shred_script.Instance.reward.ToString();
                   int a = PlayerPrefs.GetInt("Coin_value", 0) +Scene_shred_script.Instance.reward;
                    PlayerPrefs.SetInt("Coin_value", a);
                    //Level_script.I.add_xp();

                    //remaining

                }
                else
                {
                    LosePanel.SetActive(true);
                    LosePanel.GetComponent<LosePanel_Script>().u_name.text = Scene_shred_script.Instance.U_name;
                    LosePanel.GetComponent<LosePanel_Script>().Lose_coin_value.text = "-" + Scene_shred_script.Instance.fee.ToString();
                    int a = PlayerPrefs.GetInt("Coin_value", 0) - Scene_shred_script.Instance.fee;
                    PlayerPrefs.SetInt("Coin_value", a);
                }
                GameOver.Invoke(CurrentPlayer);
                gameState = GameState.NONE;
                return;
            }

            currentPlayerID = (currentPlayerID + 1) % Players.Count;
            if (currentPlayerID == 0)
            {
                RoundCount++;
                TryStopSkipping();

                if (IsGameADraw())
                {
                    GameOver.Invoke(null);
                    gameState = GameState.NONE;
                    return;
                }
            }

            if (Tb.I.CardStack.CardCount == 0)
            {
                //var discardedCards = Tb.I.DiscardStack.RecycleDiscardedCards();
                //Tb.I.CardStack.Restock(discardedCards, false);
                Debug.Log("currentplayer" + CurrentPlayer);
               
                //out of tile
                GameOver.Invoke(null);
                gameState = GameState.NONE;
                return;
            }

            drawWaitStartTime = Time.time;
            gameState = GameState.DRAWWAIT;
        }

        private void TryStopSkipping()
        {
            if (skippingDone || SkipUntilRound <= 0 || RoundCount < SkipUntilRound)
                return;
            skippingDone = true;
            AnimateCardMovement = true;
            PlayWaitDuration = tmpPlayerWaitDuration;
            DrawWaitDuration = tmpDrawWaitDuration;
            GameSpeed = DefaultGameSpeed;
        }

        /// <summary>
        /// Returns whether the current game is a draw and cannot be won by any player
        /// </summary>
        private bool IsGameADraw()
        {
            if (Players.Any(p => p.HandCardCount >= 3))
                return false;

            foreach (var p in Players)
            {
                var cardSpots = p.GetPlayerCardSpots();
                if (cardSpots.Any(spot => !spot.IsFull(true)))
                    return false;
            }
            return true;
        }

        public void TogglePause()
        {
            if (GameSpeed > 0)
            {
                DefaultGameSpeed = GameSpeed;
                GameSpeed = 0;
            }
            else
            {
                GameSpeed = DefaultGameSpeed;
            }
        }

        public List<CardSpot> GetAllCardSpots()
        {
            var cardSpots = new List<CardSpot>();
            foreach (var player in Players)
                cardSpots.AddRange(player.GetPlayerCardSpots());
            return cardSpots;
        }

        public List<Card> GetAllCardSpotCards()
        {
            var cards = new List<Card>();
            var cardSpots = GetAllCardSpots();
            foreach (var spot in cardSpots)
                cards.AddRange(spot.Objects);
            return cards;
        }

        public List<Card> GetAllCardSpotCards_gameboard()
        {
            var cards = new List<Card>();
            //var cardSpots = GetAllCardSpots();
            //foreach (var spot in cardSpots)
            //    cards.AddRange(spot.Objects);
            cards = GameBoard.Instance.Return_Gameboardcards();
            //    Debug.Log(CurrentPlayer);
            //foreach(Card a in cards)
            //{
            //    Debug.Log(a);
            //}
            return cards;
        }


        public void Log(string message, LogType type)
        {
            string prefix = "[Seed " + Seed + ", Round " + RoundCount + "] ";
            switch (type)
            {
                case LogType.Error:
                    Debug.LogError(prefix + message);
                    break;

                case LogType.Warning:
                    Debug.LogWarning(prefix + message);
                    break;

                default:
                    Debug.Log(prefix + message);
                    break;
            }
        }

        public void debug_value(int value)
        {
            Debug.Log("value" + value);
        }

        public void logger(string a)
        {
            Debug.Log(a);
        }
    }
}
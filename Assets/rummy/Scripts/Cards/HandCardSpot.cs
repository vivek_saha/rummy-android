﻿using rummy.Utility;
using System.Collections.Generic;
using UnityEngine;

namespace rummy.Cards
{
    public class HandCardSpot : RadialLayout<Card>
    {
        [SerializeField]
        public List<Card> Objects_list = new List<Card>(36);

        public override List<Card> Objects { get; set; } = new List<Card>(36);
        public bool HasCards => Objects.Count > 0;
        public bool draw_card_bool = false;

        public Transform Upspot, Downspot;

        [Tooltip("The factor by which cards will be scaled when added to the spot. When removed, the scaling is undone")]
        public float CardScale = 1.0f;

        protected override void InitValues()
        {
            zIncrement = 0.01f;
        }

        public virtual void AddCard(Card card)
        {
            //Debug.Log(Objects.Count + "=" + Objects_list.Count);
            //if (Objects.Count!= Objects_list.Count)
            //{
            //    Debug.Log("here in not equal");
            //    Objects_list.Clear();
            //    Objects_list = Objects;
            //}
            if (card != null)
            {
                if (Objects.Contains(card))
                    throw new RummyException("CardSpot " + gameObject.name + " already contains " + card);
                AddCard(card, Objects.Count);
            }
            else
            {
                //Debug.Log(Objects.Count + "=" + Objects_list.Count);
                Objects.Insert(Objects.Count, card);
                Objects_list.Insert(Objects_list.Count, card);
            }
        }

        public void AddCard(Card card, int index)
        {
            //Debug.Log(index);
            if (Objects.Count - 1 > index)
            {
                if (Objects[index] == null)
                {
                    Objects[index] = card;
                }
            }
            else
            {
                Objects.Insert(index, card);
                Objects_list.Insert(index, card);
            }
            GameObject gm_obj;
            //Debug.Log(transform.parent.name);
            if (transform.parent.name == "CardSpotsNode")
            {
                card.transform.SetParent(transform, true);
            }
            else
            {
                if (transform.parent.GetComponent<Player>().player_type_ == Player_Type.User)
                {
                    if (draw_card_bool)
                    {
                        if (index > 17)
                        {
                            //  bot handcardspot
                            gm_obj = Downspot.GetChild(index - 17).gameObject;
                            card.transform.SetParent(Downspot.GetChild(index - 17).transform, true);
                            gm_obj.GetComponent<GameBoardTile>().filled = true;
                            //card.transform.localPosition = Vector3.one;
                        }
                        else if (index < 18)
                        {
                            //top handcardspot
                            gm_obj = Upspot.GetChild(index + 1).gameObject;
                            card.transform.SetParent(Upspot.GetChild(index + 1).transform, true);
                            gm_obj.GetComponent<GameBoardTile>().filled = true;
                            //card.transform.localPosition = Vector3.one;
                        }
                    }
                }
                else if (transform.parent.GetComponent<Player>().player_type_ == Player_Type.AI)
                {
                    card.transform.SetParent(transform, true);
                }
            }
            draw_card_bool = false;
            //gm_obj.GetComponent<SpriteRenderer>().sprite = card.GetComponent<SpriteRenderer>().sprite;
            card.transform.localPosition = Vector3.forward * (-1);

            card.transform.localScale = card.transform.localScale * CardScale;
            //var carrrd = card.transform.gameObject.GetComponent<BoxCollider>();
            //if (carrrd == null)
            //{
            //    carrrd = card.transform.gameObject.AddComponent<BoxCollider>();
            //}
            UpdatePositions();
        }

        public void RemoveCard(Card card)
        {
            Objects.Remove(card);
            Objects_list.Remove(card);
            //Debug.Log(Objects.Count + "=" + Objects_list.Count);
            card.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            //card.transform.SetParent(null, true);
            UpdatePositions();
        }

        //public void arrange_cards()
        //{
        //    //List
        //    var combos =
        //}

        public virtual List<Card> ResetSpot()
        {
            var cards = new List<Card>(Objects);
            while (Objects.Count > 0)
            {
                RemoveCard(Objects[0]);
            }
            return cards;
        }

        public void REmove_Null_cards()
        {
            foreach (Card a in Objects)
            {
                if (a == null)
                {
                    RemoveCard(a);
                }
            }
        }
    }
}
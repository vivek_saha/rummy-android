﻿using DG.Tweening;
using rummy.Utility;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace rummy.Cards
{
    //[System.Serializable]
    public class Card : MonoBehaviour
    {
        private Ray ray;
        private RaycastHit hit;
        public Vector3 pre_pos;
        private bool setter_1 = true;
        private Color clr = new Color(0, 1, 0);
        public bool isdragging = false;
        public bool backer = false;
        //public float size_gameboard;
        public bool undoing_card = false;

        #region defs

        public enum CardColor
        {
            BLACK = 0,
            RED = 1
        }

        /// <summary> The number of different card ranks in the game </summary>
        public static readonly int CardRankCount = 13;

        public enum CardRank
        {
            JOKER = 0,
            ACE = 1,
            TWO = 2,
            THREE = 3,
            FOUR = 4,
            FIVE = 5,
            SIX = 6,
            SEVEN = 7,
            EIGHT = 8,
            NINE = 9,
            TEN = 10,
            JACK = 11,
            QUEEN = 12,
            KING = 13
        }

        /// <summary> The number of different card suits in the game </summary>
        public static readonly int CardSuitCount = 4;

        public enum CardSuit
        {
            HEARTS = 1,
            DIAMONDS = 2,
            SPADES = 3,
            CLUBS = 4
        }

        public static readonly IDictionary<CardRank, int> CardValues = new Dictionary<CardRank, int>
        {
            { CardRank.JOKER,   0 },
            { CardRank.ACE,     1 },
            { CardRank.TWO,     2 },
            { CardRank.THREE,   3 },
            { CardRank.FOUR,    4 },
            { CardRank.FIVE,    5 },
            { CardRank.SIX,     6 },
            { CardRank.SEVEN,   7 },
            { CardRank.EIGHT,   8 },
            { CardRank.NINE,    9 },
            { CardRank.TEN,     10 },
            { CardRank.JACK,    10 },
            { CardRank.QUEEN,   10 },
            { CardRank.KING,    10 }
        };

        public static readonly IDictionary<CardRank, string> RankLetters = new Dictionary<CardRank, string>
        {
            { CardRank.JOKER,   "?"},
            { CardRank.ACE,     "1" },
            { CardRank.TWO,     "2" },
            { CardRank.THREE,   "3" },
            { CardRank.FOUR,    "4" },
            { CardRank.FIVE,    "5" },
            { CardRank.SIX,     "6" },
            { CardRank.SEVEN,   "7" },
            { CardRank.EIGHT,   "8" },
            { CardRank.NINE,    "9" },
            { CardRank.TEN,     "10" },
            { CardRank.JACK,    "J" },
            { CardRank.QUEEN,   "Q" },
            { CardRank.KING,    "K" }
        };

        public static readonly IDictionary<CardSuit, char> SuitSymbols = new Dictionary<CardSuit, char>
        {
#if UNITY_WEBGL
            { CardSuit.HEARTS,   'h' },
            { CardSuit.DIAMONDS, 'd' },
            { CardSuit.CLUBS,    'c' },
            { CardSuit.SPADES,   's' }
#else
            { CardSuit.HEARTS,   '♥' },
            { CardSuit.DIAMONDS, '♦' },
            { CardSuit.CLUBS,    '♣' },
            { CardSuit.SPADES,   '♠' }
#endif
        };

        private static char GetSuitSymbol(Card card)
        {
            if (card.IsJoker())
                return card.IsBlack() ? 'b' : 'r';
            return SuitSymbols[card.Suit];
        }

        public static List<CardSuit> GetOtherTwo(CardSuit s1, CardSuit s2)
        {
            if (s1 == s2)
                throw new RummyException("GetOtherTwo got the same CardSuit twice");
            var suits = new List<CardSuit>() { CardSuit.HEARTS, CardSuit.DIAMONDS, CardSuit.SPADES, CardSuit.CLUBS };
            suits.Remove(s1);
            suits.Remove(s2);
            return suits;
        }

        #endregion defs

        public CardRank Rank = CardRank.TWO;
        public CardSuit Suit = CardSuit.HEARTS;

        public void SetType(CardRank rank, CardSuit suit)
        {
            Rank = rank;
            Suit = suit;
            TypeChanged.Invoke();
        }

        public CardColor Color
        {
            get
            {
                if (Suit == CardSuit.HEARTS || Suit == CardSuit.DIAMONDS)
                    return CardColor.RED;
                return CardColor.BLACK;
            }
        }

        public int Value
        {
            get
            {
                if (Rank == CardRank.JOKER)
                {
                    // Most of the time, joker values are calculated directly since it depends on the context
                    // Here, joker on hand is worth 0 so it will always be discarded last
                    return 0;
                }
                return CardValues[Rank];
            }
        }

        public bool IsBlack() => Color == CardColor.BLACK;

        public bool IsRed() => Color == CardColor.RED;

        public bool IsJoker() => Rank == CardRank.JOKER;

        public bool LooksLike(Card other) => Suit == other.Suit && Rank == other.Rank;


        //private bool temp_move = false;
        private bool isMoving;
        //{
        //    get { return temp_move; }
        //    set
        //    {
        //        if (temp_move)
        //        {
        //            //here
        //            if(Tb.I.GameMaster.gamestarted)
        //            {
        //                //mouse_check();
        //                onmovefinish_normal_fun();
        //            }
        //            temp_move = value;
        //        }
        //        else
        //        {
        //            temp_move = value;
        //        }
        //    }
        //}
        private Vector3 targetPos;

        public class Event_MoveFinished : UnityEvent<Card> { }

        public Event_MoveFinished MoveFinished = new Event_MoveFinished();

        public UnityEvent TypeChanged = new UnityEvent();

        public class CardStateChangedEvent : UnityEvent<bool> { }

        public CardStateChangedEvent VisibilityChanged = new CardStateChangedEvent();
        public CardStateChangedEvent HasBeenTurned = new CardStateChangedEvent();
        public CardStateChangedEvent SentToBackground = new CardStateChangedEvent();

        public void SetVisible(bool visible) => VisibilityChanged.Invoke(visible);

        /// <summary>Sets this card to show its back (turned=true) or not</summary>
        public void SetTurned(bool turned) => HasBeenTurned.Invoke(turned);

        /// <summary>Send this card behind other cards (background=true) or not</summary>
        public void SendToBackground(bool background) => SentToBackground.Invoke(background);

        public override string ToString() => RankLetters[Rank] + GetSuitSymbol(this);

        private void Start()
        {
        }

        private void onmovefinish_normal_fun(Card arg0)
        {
            //throw new NotImplementedException();
            if (Physics.Raycast(transform.position + new Vector3(0, 0f, 0.2f), Vector3.forward, out hit, Mathf.Infinity))
            {
                //mouse_check(hit);
                //Debug.Log(hit);
                mouse_check(hit);
            }
        }

        public void MoveCard(Vector3 targetPosition, bool animateMovement)
        {
            isMoving = true;
            BoxCollider bc = GetComponent<BoxCollider>();
            if (bc != null)
            {
                //bc.enabled = false;
            }
            Vector3 ter = targetPosition;
            ter.z = -1;
            targetPos = ter;
            //targetPos = targetPosition;

            if (!animateMovement)
            {
                FinishMove();
            }
            else
            {
                transform.DOMove(targetPos, Tb.I.GameMaster.CardMoveSpeed).OnComplete(() => FinishMove());

            }
        }

        private void FinishMove()
        {
            transform.position = targetPos;
            //Debug.Log(targetPos);
            //setter_1 = true;
            isMoving = false;
            //change_card_size_pos();
            //Debug.Log("here");
            //mouse_check();
            //repos_card_check();
            //normal_fun();
            MoveFinished.Invoke(this);

            //if (Tb.I.GameMaster.CurrentPlayer.player_type_ == Player_Type.User)
            {
                //if (Physics.Raycast(transform.position + new Vector3(0, 0f, 0.2f), Vector3.forward, out hit, Mathf.Infinity))
                //{
                //    //        //mouse_check(hit);
                //    //        //Debug.Log(hit);
                //    mouse_check(hit);
                //}
                //    //onmovefinish_normal_fun(gameObject.GetComponent<Card>());
                MoveFinished.AddListener(onmovefinish_normal_fun);
            }
            BoxCollider bc = GetComponent<BoxCollider>();
            if (bc != null)
            {
                GetComponent<BoxCollider>().enabled = true;
            }
        }

        private void Update()
        {
            Debug.DrawRay(transform.position + new Vector3(0, 0f, 0.2f), Vector3.forward, new Color(0, 0, 1));
            if (!isMoving)
            {

                //raycast__1();
                return;
            }

            //if (Vector3.Distance(transform.position, targetPos) <= 2 * Time.deltaTime * Tb.I.GameMaster.CardMoveSpeed)
            //{

            //    //Debug.Log("dis"+Time.deltaTime * Tb.I.GameMaster.CardMoveSpeed);
            //    FinishMove();
            //}
            //else
            //{
            //    //Vector3 s = transform.position;
            //    //s.z = -1;
            //    //transform.position = s;
            //    transform.Translate((targetPos - transform.position).normalized * Time.deltaTime * Tb.I.GameMaster.CardMoveSpeed, Space.World);
            //}

        }

        private void finallyfinished()
        {
            //one_time_set = false;
            //throw new NotImplementedException();
            Debug.Log("finallyfinished");
            onmovefinish_normal_fun(gameObject.GetComponent<Card>());
        }

        //public void OnMouseDrag()
        //{
        //    Vector3 temmp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //    GetComponent<BoxCollider>().enabled = false;
        //    temmp.z = -1;
        //    transform.position = temmp;
        //    //Debug.Log("called");

        //}
        private void OnMouseUp()
        {
            //Debug.Log("false");
            isdragging = false;
            GetComponent<BoxCollider>().enabled = true;
        }

        //private void OnMouseUpAsButton()
        //{
        //    this.mouseup = true;
        //    Debug.Log("called");

        //}
        //public void OnEndDrag(PointerEventData eventData)
        //{
        //    this.mouseup = true;
        //}
        private void OnMouseDown()
        {
            transform.position = pre_pos;
        }

        public void raycast__1()
        {
            //if (isdragging)
            //{
            //    Vector3 temmp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //    GetComponent<BoxCollider>().enabled = false;
            //    temmp.z = -1;
            //    transform.position = temmp;
            //}
            //Debug.DrawRay(transform.position, Vector3.forward, clr);
            //float xx = GetComponent<SpriteRenderer>().bounds.size.x / 2;
            //float yy = GetComponent<SpriteRenderer>().bounds.size.y / 2;
            //Debug.DrawRay(transform.position + (new Vector3(xx, yy, 0)), Vector3.forward, new Color(1, 0, 0));
            //Debug.DrawRay(transform.position + (new Vector3(-xx, yy, 0)), Vector3.forward, new Color(1, 0, 0));
            //Debug.DrawRay(transform.position + (new Vector3(xx, -yy, 0)), Vector3.forward, new Color(1, 0, 0));
            //Debug.DrawRay(transform.position + (new Vector3(-xx, -yy, 0)), Vector3.forward, new Color(1, 0, 0));
            //if (!isMoving)
            //{
            //    if (Physics.Raycast(transform.position, Vector3.forward, out hit, Mathf.Infinity))
            //    {
            //        //mouse_check(hit);
            //    }
            //    else
            //    {
            //        //if (Input.GetMouseButtonUp(0))
            //        //{
            //        //    if (isdragging)
            //        //    {
            //        //        Debug.Log("backtopast");
            //        //        isdragging = false;
            //        //        MoveCard(pre_pos, true);
            //        //        Debug.Log(pre_pos);
            //        //    }
            //        //}
            //    }
            //}
            //else
            //{
            //    //Debug.Log(!isMoving);
            //}
        }

        public void normal_fun()
        {
            //Debug.Log("in normal  fun");
            if (Physics.Raycast(transform.position + new Vector3(0, 0f, 0.2f), Vector3.forward, out hit, Mathf.Infinity))
            {
                //mouse_check(hit);
                //Debug.Log(hit);
                mouse_check(hit);
            }
            else
            {
                isdragging = false;
                Vector3 abc = pre_pos;
                abc.z = -1;
                MoveCard(abc, true);
                //Debug.Log(pre_pos);
            }
        }


        public void change_card_size_pos()
        {
            Vector3 ter = targetPos;
            ter.z = -1;
            transform.position = ter;
            Drag_drop_script.I.set_initial_size_card(gameObject.GetComponent<Card>());
            transform.localScale = transform.localScale * GameBoard.Instance.card_size_gameboard;
            //if()
        }
        public void mouse_check(RaycastHit hit)
        {
            //if (Input.GetMouseButtonUp(0) )
            //{
            //setter_1 = false;
            //Debug.Log("hit");
            //print(isdragging);
            if (hit.collider.GetComponent<GameBoardTile>() != null)
            {
                if (hit.collider.gameObject.transform.childCount == 0)
                {
                    hit.collider.GetComponent<GameBoardTile>().filled = false;
                }
                else if(hit.collider.gameObject.transform.GetChild(0).gameObject == this.gameObject)
                {
                    hit.collider.GetComponent<GameBoardTile>().filled = false;
                }
                if (hit.collider.GetComponent<GameBoardTile>().filled)
                {
                    //filled tile
                    isdragging = false;
                    MoveCard(pre_pos, true);
                }
                else
                {
                    //emptytile
                    //Debug.Log(hit.collider.name);
                    //hit.collider.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
                    hit.collider.GetComponent<GameBoardTile>().filled = true;
                    gameObject.transform.SetParent(hit.collider.gameObject.transform);
                    //hit.collider.GetComponent<GameBoardTile>().release_adder_card(this.gameObject);

                    if (!undoing_card)
                    {
                        UndoRedoSystem.I.TrackChangeAction(UndoableActionType.parenttransform, this.gameObject, pre_pos, hit.collider.transform.position);
                        Gamplay_panel_script.I.set_undo_but(true);
                    }
                    if (hit.collider.GetComponent<GameBoardTile>().Gameboard_card)
                    {
                        //Tb.I.hdcspot.RemoveCard(this.gameObject.GetComponent<Card>());
                        //hit.collider.GetComponent<SpriteRenderer>().sprite = GetComponent<SpriteRenderer>().sprite;
                        //gameObject.SetActive(false);
                        transform.position = hit.collider.gameObject.transform.position;
                        Vector3 ter = hit.collider.gameObject.transform.position;
                        ter.z = -1;
                        transform.position = ter;
                        Drag_drop_script.I.set_initial_size_card(gameObject.GetComponent<Card>());
                        transform.localScale = transform.localScale * GameBoard.Instance.card_size_gameboard;
                    }
                    else
                    {
                        //UndoRedoSystem.I.TrackChangeAction(UndoableActionType.parenttransform, this.gameObject, pre_pos, hit.collider.transform.position);
                        Vector3 ter = hit.collider.gameObject.transform.position;
                        ter.z = -1;
                        transform.position = ter;
                        //add pos of the index card
                        int iindex = int.Parse(hit.collider.gameObject.name);
                        Debug.Log(iindex);
                        //if (Tb.I.hdcspot.Objects.Contains(GetComponent<Card>()))
                        //{
                        //}
                        //else
                        //{
                        Tb.I.hdcspot.AddCard(GetComponent<Card>());
                        //}
                    }
                    //UndoRedoSystem.I.undo_move = false;
                    undoing_card = false;
                    isdragging = false;
                }
                //}
            }
            else
            {
                isdragging = false;
                MoveCard(pre_pos, true);
            }
        }

        public void set_prepos()
        {
            pre_pos = transform.position;
            //Debug.Log(pre_pos);
        }

        public void set_prepos(Vector3 a)
        {
            pre_pos = a;
            //Debug.Log(pre_pos);
        }

        public void repos_card_check()
        {
            setter_1 = true;
        }

        private void OnEnable()
        {
            setter_1 = true;
        }
    }
}
﻿using UnityEngine;

public class GameBoardTileGenarator : MonoBehaviour
{
    public GameObject prefab;
    public Transform Container;

    public GameBoardTile GenerateTile(int x, int y)
    {
        GameObject spawnedTile = Instantiate(prefab, Container);
        var boardTile = spawnedTile.GetComponent<GameBoardTile>();
        if (boardTile == null)
        {
            boardTile = spawnedTile.AddComponent<GameBoardTile>();
        }
        //Debug.Log(boardTile);
        //Debug.Log(x + " " + y);
        boardTile.x = x;
        boardTile.y = y;

        boardTile.filled = false;
        return boardTile;
    }

    public void set_container()
    {
        Container.position = new Vector3(-27.13f, -4.09f, 0);
    }
}
﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Update_API : MonoBehaviour
{
    public static Update_API Instance;

    public long credits;
    public int xp;
    public string username;
    public int is_update = 0;
    public int is_profile = 0;
    string rawJson;
    public string type;
    public string order_id;

    public JSONNode jsonResult;
    public bool playagain = false;
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void Update_data()
    {
        if (is_update == 1)
        {
            //Debug.Log(PlayerPrefs.GetString("Slot_credits"));
            //long coin_chk = (long.Parse(PlayerPrefs.GetString("Slot_credits", "0")) - Gamemanager.Instance.coin_change_checker);
            //int xp_chk = PlayerPrefs.GetInt("CurrentXP", 0) - Gamemanager.Instance.xp_change_checker;
            //if (coin_chk != 0 || xp_chk != 0)
            //{
            //    credits = coin_chk;
            //    xp = xp_chk;
            StartCoroutine("Upload");
            //}
        }
        else
        {
            StartCoroutine("Upload");
        }
    }
    IEnumerator Upload()
    {
        WWWForm form = new WWWForm();
        form.AddField("is_profile", is_profile);
        form.AddField("is_update", is_update);
        if (is_update == 1)
        {
            form.AddField("type", type);
            if (type == "purchase")
            {
                form.AddField("order_id", order_id);
            }

            Scene scene__1 = SceneManager.GetActiveScene();
            if (scene__1.name == "Gameplay")
            {
                //Gamplay_panel_script.I.
                //Scene_shred_script.Instance.
                form.AddField("wallet_coin", credits.ToString());

                form.AddField("current_xp", xp);

            }
            else
            {
                //Debug.Log("Before _sending : " + GameManager.Instance.Coin_var);

                form.AddField("wallet_coin", GameManager.Instance.Coin_var);

                form.AddField("current_xp", GameManager.Instance.Level_var);
            }
        }
        if (is_profile == 1)
        {
            form.AddField("name", username);
            form.AddBinaryData("profile_picture", edit_profile_panel.I.itemBGBytes, "temp.png", "image/png");
        }
        UnityWebRequest www = UnityWebRequest.Post("http://104.131.36.193:6800/api/update", form);
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        yield return www.SendWebRequest();
        Debug.Log("www: " + www.responseCode);
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            Check_Internet_connection();
        }
        else
        {
            Scene scene__1 = SceneManager.GetActiveScene();
            if (scene__1.name != "Gameplay")
            close_Retry_panel();

            rawJson = www.downloadHandler.text;
            Debug.Log("Update_API_data upload complete!" + rawJson);
            //Get_response_data();
            //Scene scene__1 = SceneManager.GetActiveScene();
            if (scene__1.name == "Gameplay")
            {
                //Gamplay_panel_script.I.
                if (playagain)
                {
                    playagain = false;
                }
                else
                {
                    Scene_shred_script.Instance.Upload_data_server_done();
                }
            }
            else
            {
                SEt_updated_profile();
            }
        }

    }



    public void SEt_updated_profile()
    {
        jsonResult = JSON.Parse(rawJson);

        if (is_profile == 1)
        {

            username = jsonResult["data"]["name"].Value;
            Homepanel_script.I.set_username(username);
            edit_profile_panel.I.profile_pic_link_2 = jsonResult["data"]["profile_picture"].Value;
            edit_profile_panel.I.Get_image();
            //PlayerPrefs.SetString("Username", Set_User_data.Instance.Username_server);
            //PlayerPrefs.SetString("profile_pic_link_2", jsonResult["data"]["profile_picture"].Value);

        }
        else if (is_update == 1)
        {
            long temp_cre = credits;
            credits = jsonResult["data"]["wallet_coin"].AsLong;
            Homepanel_script.I.set_Coin(credits.ToString());
            //type = jsonResult["data"]["type"].Value;

            //Gamemanager.Instance.coin_change_checker = credits;
            //PlayerPrefs.SetString("Slot_credits", credits.ToString());
            //if (Gamemanager.Instance.Gameplay_panel.activeSelf)
            //{
            //    Lvl_manager.Instance.OnPurchase_coins(temp_cre);
            //}
            //else if (Gamemanager.Instance.home_panel.activeSelf)
            //{
            //    if (type == "purchase")
            //    {

            //        Gamemanager.Instance.transform.GetComponent<Coin_effect_home_panel>().on_buy_coin();
            //        Gamemanager.Instance.transform.GetComponent<Coin_effect_home_panel>().credits_value = temp_cre;
            //        //Gamemanager.Instance.transform.GetComponent<Floating_coin_script>().Move_up("+"+temp_cre);
            //    }
            //    else if (type == "watch_video")
            //    {
            //        Gamemanager.Instance.transform.GetComponent<Coin_effect_home_panel>().on_watch_video_add();
            //        Gamemanager.Instance.transform.GetComponent<Coin_effect_home_panel>().credits_value = temp_cre;
            //        //Gamemanager.Instance.transform.GetComponent<Floating_coin_script>().Move_up("+" + temp_cre);
            //    }
            //    //Invoke("wait_for_eeffeecctt_homepanel", 1f);            
            //}
            //else
            //{
            //    Gamemanager.Instance.Setting_vlaues();
            //}
            xp = jsonResult["data"]["current_xp"].AsInt;
            Homepanel_script.I.set_lvl(xp.ToString());
            //Gamemanager.Instance.xp_change_checker = xp;
        }
        //if (is_profile == 1)
        //{
        //    Set_User_data.Instance.Set_username();
        //    Set_User_data.Instance.Get_saved_profile_pic();
        //}
        is_profile = 0;
        is_update = 0;
    }

    //public void wait_for_eeffeecctt_homepanel()
    //{
    //    Gamemanager.Instance.Setting_vlaues();
    //}
    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    Notice_panel_script.Instance.notice_Panel.SetActive(true);
                    Notice_panel_script.Instance.Something_wentwrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    Notice_panel_script.Instance.notice_Panel.SetActive(true);
                    Notice_panel_script.Instance.No_internet_Panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.No_internet_Panel.SetActive(true);
                }
            }
        }));
    }

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }

    //public bool check_net()
    //{
    //    WWW www = new WWW("http://www.google.com");
    //    //yield return www;
    //    if (www.error != null)
    //    {
    //        return false;
    //    }
    //    else
    //    {
    //        return true;
    //    }

    //}
    public void Retry_connection()
    {
        Update_data();
        Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(false);
        Notice_panel_script.Instance.No_internet_Panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSeconds(3f);
        //StartCoroutine("check_connection");
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        Notice_panel_script.Instance.notice_Panel.SetActive(false);
        Update_data();
    }
    public void close_Retry_panel()
    {
        Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(false);
        Notice_panel_script.Instance.No_internet_Panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        Notice_panel_script.Instance.notice_Panel.SetActive(false);
    }
}

﻿using UnityEngine;

public class Mouse_hover_select : MonoBehaviour
{
    private Ray ray;
    private RaycastHit hit;
    public Color clr;

    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // hit =;
        Debug.DrawRay(ray.origin, ray.direction, Color.green);
        if (Physics.Raycast(ray.origin, ray.direction, out hit, Mathf.Infinity))
        {
            print(hit.collider.name);
            if (hit.collider.GetComponent<GameBoardTile>() != null)
            {
                hit.collider.GetComponent<SpriteRenderer>().color = clr;
            }
            //Debug.Log("nothing hit");
        }
        else
        {
        }
    }
}
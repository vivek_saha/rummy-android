﻿using rummy;
using rummy.Cards;
using rummy.Utility;
using System.Collections.Generic;
using UnityEngine;
using static rummy.Player;

[System.Serializable]
public class Boards_card_check
{
    public List<Card> check_list;
}

public class GameBoard : MonoBehaviour
{
    public static GameBoard Instance;
    public GameBoardTileGenarator generator;
    public Player player_obj;
    public int width;
    public int height;
    private GameBoardTile[,] boardPieces;
    public int existing_counter;
    public float card_size_gameboard = 0.75f;
    public bool gameboard_card_settting = false;


    public bool Automeld_bool = false;
    public GameBoardTile[,] Boardpieces { get { return boardPieces; } }

    //public Boards_card_check bd = new Boards_card_check();
    [SerializeField]
    public List<List<Card>> nestedList = new List<List<Card>>();

    public List<List<Card>> FinalizeboardList = new List<List<Card>>();

    public List<Card> check_list;
    //public List<List<Card>> ac;
    //public List<Card> Boards_card_check;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    private void Start()
    {
        boardPieces = new GameBoardTile[width, height];
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                boardPieces[x, y] = generator.GenerateTile(x, y);
            }
        }
        Invoke("set_generator_contauiner", 1f);
    }

    public void set_generator_contauiner()
    {
        generator.set_container();
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void OnReady_click()
    {
        fill_nestedList();

        //Debug.Log(nestedList[0][0]);
        //foreach (List<Card> x in nestedList)
        //{
        //    foreach (Card a in x)
        //    {
        //        Debug.Log(a);
        //    }
        //}

        if (nestedList.Count == 0)
        {
            //draw card
            Draw_card();
        }
        else
        {

            //Debug.Log("existing_counter : " + existing_counter + " Handcard : " + Tb.I.hdcspot.Objects.Count);
            if (existing_counter == -1 || existing_counter == Tb.I.hdcspot.Objects.Count)
            {
                //player hand card counter check
                Draw_card();
            }
            else
            {
                if (Checker_nestedlist())
                {
                    Debug.Log("succed");
                    if(player_obj.Initial_meld)
                    {
                        //if (arrange_cards_script.I.Set_value >= 30)
                        //{
                        player_obj.Initial_meld = false;
                        arrange_cards_script.I.value.gameObject.SetActive(false);
                        arrange_cards_script.I.sp.gameObject.SetActive(true);
                        //}
                    }
                    // TODO: Find permenant laydown card list
                    // TODO: Add to permenant laydown card list
                    FinalizeboardList = nestedList;
                    player_obj.State = PlayerState.IDLE;
                    Tb.I.GameMaster.PlayerFinished();
                    //player_obj.laydownCards.
                }
                else
                {
                    Debug.Log("fail");
                    Draw_card();
                }
            }
        }
    }

    public void fill_nestedList()
    {
        nestedList = new List<List<Card>>();
        //1 check all cards
        for (int y = 0; y < height; y++)
        {
            check_list = new List<Card>();
            //bd.check_list.Clear();
            for (int x = 0; x < width; x++)
            {
                if (boardPieces[x, y].filled)
                {
                    check_list.Add(boardPieces[x, y].transform.GetChild(0).GetComponent<Card>());
                    if (x == width - 1)
                    {
                        Add_to_nested_list();
                    }
                }
                else
                {
                    if (check_list.Count > 0)
                    {
                        Add_to_nested_list();
                    }
                }
            }
        }
    }


    public List<Card> Return_Gameboardcards()
    {
        fill_nestedList();
        var cards = new List<Card>();
        foreach (var spot in nestedList)
            cards.AddRange(spot);
        return cards;
    }
    private void Draw_card()
    {
        player_obj.DrawCard(false);
        player_obj.State = PlayerState.IDLE;
        Tb.I.GameMaster.PlayerFinished();
    }

    public void Add_to_nested_list()
    {
        //Debug.Log(bd.check_list.Count);
        //foreach(Card a in bd.check_list)
        //{
        //    Debug.Log(a);
        //}
        //var temp = bd;
        nestedList.Add(check_list);
        //Debug.Log(nestedList[0].check_list[0]);
        //bd.check_list.Clear();
        check_list = new List<Card>();
    }

    public bool Checker_nestedlist()
    {
        for (int list = 0; list < nestedList.Count; list++)
        {
            //Debug.Log(nestedList[list][0]);
            if (nestedList[list].Count < 3)
            {
                //return false
                return false;
            }

            //var combos = CardUtil.GetAllUniqueCombos(nestedList[list], null);
            //List<Card> ac = new List<Card>();
            //foreach (var a in combos)
            //{
            //    Debug.Log(a.GetCards());
            //    //Debug.Log(a);
            //    //a.Sets

            //}
            //Debug.Log(combos.Count);
            //ac = combos[1].GetCards();
            for (int cd = 0; cd < nestedList[list].Count; cd++)
            {
                if (nestedList[list].Count > 4)
                {
                    //check long run sequence
                    if (Run.IsValidRun(nestedList[list]))
                    {
                    }
                }
                else
                {
                    //check for sets and runs
                    if (Run.IsValidRun(nestedList[list]))
                    {
                        return true;
                    }
                    else if (Set.IsValidSet(nestedList[list]))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                    //if (nestedList[list][cd] == ac[cd])
                    //{
                    //}
                    //else
                    //{
                    //    //return false
                    //    return false;
                    //}

                    //if (nestedList[list][cd] == combos[0].GetCards())
                    //{
                    //}
                }
            }
        }
        return true;
    }

    public bool Board_card_add(List<Card> cl)
    {
        gameboard_card_settting = true;
        //Debug.Log(Tb.I.GameMaster.CurrentPlayer.player_type_);

        //space counter 
        int counter_space = 0;
        //counter_cards_combos
        //int ccc_counter = 0;

        // main list card counter
        //int cardcounter = 0;
        for (int y = 0; y < height; y++)
        {
            bool one_space = true;
            counter_space = 0;
            check_list = new List<Card>();
            //bd.check_list.Clear();
            for (int x = 0; x < width; x++)
            {
                if (boardPieces[x, y].filled || boardPieces[x, y].transform.childCount > 0)
                {
                    counter_space = 0;
                    one_space = true;
                }
                else
                {
                    //if for skipping one space every sets and runs

                    if (x == 0)
                    {
                        counter_space++;
                        one_space = false;
                    }
                    else
                    {
                        if (one_space)
                        {
                            one_space = false;
                        }
                        else
                        {
                            counter_space++;
                        }
                    }

                }
                //for(int m = 0; m < cadcom.Sets.Count;m++)
                //{

                //}

                if (counter_space == cl.Count)
                {
                    one_space = true;

                    int start_value = x - cl.Count + 1;
                    //add the set to board
                    for (int i = 0; i < cl.Count; i++)
                    {
                        if (Tb.I.GameMaster.CurrentPlayer.player_type_ == Player_Type.User)
                        {
                            cl[i].gameObject.transform.SetParent(boardPieces[start_value, y].transform);
                            Vector3 ter = boardPieces[start_value, y].transform.position;
                            ter.z = -1;

                            //cl[i].transform.position = ter;
                            cl[i].MoveCard(ter, true);
                        }
                        else
                        {



                            boardPieces[start_value, y].GetComponent<GameBoardTile>().filled = true;

                            cl[i].gameObject.transform.SetParent(boardPieces[start_value, y].transform);
                            //cl[i].transform.position = boardPieces[start_value, y].transform.position;
                            Vector3 ter = boardPieces[start_value, y].transform.position;
                            ter.z = -1;

                            //cl[i].transform.position = ter;
                            cl[i].MoveCard(ter, true);
                            Drag_drop_script.I.set_initial_size_card(cl[i]);
                            cl[i].transform.localScale = cl[i].transform.localScale * card_size_gameboard;
                        }
                        start_value++;
                        if (i == cl.Count - 1)
                        {
                            gameboard_card_settting = false;
                            return true;
                        }
                    }
                    //return false;
                    //break;
                    //ccc_counter++;
                    //if (ccc_counter == counter_cards_combos.Count)
                    //{
                    //}

                }
            }
        }
        gameboard_card_settting = false;
        return false;
        //}
    }


    public void refresh_gameboard()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (boardPieces[x, y].transform.childCount == 0)
                {
                    boardPieces[x, y].GetComponent<GameBoardTile>().filled = false;
                }
            }
        }
    }

    //public bool CanFit(Card newCard,List<Card> Objects,  , out Card Joker)
    //{
    //    Joker = null;
    //    if (Type == SpotType.NONE)
    //        return false;
    //    if (Type == SpotType.SET)
    //        return new Set(Objects).CanFit(newCard, out Joker);
    //    return new Run(Objects).CanFit(newCard, out Joker);
    //}
}
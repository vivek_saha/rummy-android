﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Homepanel_script : MonoBehaviour
{

    public static Homepanel_script I;

    public GridLayoutGroup grid_layout;
    public Image profile_pic;
    public Image profile_pic_profile_panel;
    //public Image gm_profile_pic;
    public Text u_name;
    public Text u_name_profile_panel;
    public Text wallet_coin;
    public Text wallet_coin_profilepanel;
    public Text LVL;
    public Text LVLV_COPY;
    public Button[] lvl_butns;
    public GameObject[] locks;


    //public GameObject Shoppanel;

    private void OnEnable()
    {
        //Time.timeScale = 1;
        //GameManager.Instance.start_game_again();
        Update_lvl_lockes();
    }



    private void Awake()
    {
        I = this;
    }


    public void Update_lvl_lockes()
    {
        for (int i = 0; i < lvl_butns.Length; i++)
        {
            if (i > GameManager.Instance.Level_var - 1)
            {
                lvl_butns[i].interactable = false;
                locks[i].SetActive(true);
            }
            else
            {
                lvl_butns[i].interactable = true;
                locks[i].SetActive(false);
            }
        }


    }

    public void set_username(string a)
    {
        u_name.text = a;
        u_name_profile_panel.text = a;
        GameManager.Instance.U_name = a;
    }

    public void set_profile_pic(Sprite a)
    {
        profile_pic.sprite = a;
        profile_pic_profile_panel.sprite = a;
        GameManager.Instance.gm_profile_pic.sprite = a;
    }

    public void set_lvl(string a)
    {
        LVL.text = a;
        LVLV_COPY.text = a;
    }

    public void set_Coin(string a)
    {
        wallet_coin.text = a;
        wallet_coin_profilepanel.text = a;
    }




    // Start is called before the first frame update
    private void Start()
    {

        ratioSetter();
    }

    // Update is called once per frame
    private void Update()
    {
    }



    public void ratioSetter()
    {
        if (Camera.main.aspect >= 1.7)
        {
            //Debug.Log("16:9");

            grid_layout.cellSize = new Vector2(grid_layout.cellSize.x-((Camera.main.aspect-1.7f)*50), grid_layout.cellSize.y - ((Camera.main.aspect - 1.7f) * 100));
        }
        else if (Camera.main.aspect >= 1.5)
        {
            Debug.Log("3:2");
        }
        else
        {
            Debug.Log("4:3");
        }
    }

    public void Share_link()
    {
        GameManager.Instance.Share();
    }
}
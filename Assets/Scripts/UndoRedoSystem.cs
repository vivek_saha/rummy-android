﻿using rummy.Cards;
using rummy.Utility;
using System.Collections.Generic;
using UnityEngine;

public class UndoableAction
{
    public UndoableActionType Type;
    public UnityEngine.Object Target;
    public object From;
    public object To;

    public UndoableAction(UndoableActionType type, UnityEngine.Object target, object from, object to)
    {
        Type = type;
        Target = target;
        From = from;
        To = to;
    }
}

public enum UndoableActionType
{
    Enable,
    SetActive,
    Position,
    transform,
    parenttransform,
    automeld

    // ... according to your needs
}

public class UndoRedoSystem : MonoBehaviour
{
    public static UndoRedoSystem I;
    private Stack<UndoableAction> availableUndos = new Stack<UndoableAction>();
    private Stack<UndoableAction> availableRedos = new Stack<UndoableAction>();
    //public bool undo_move = false;

    private void Awake()
    {
        I = this;
    }

    public void TrackChangeAction(UndoableActionType type, UnityEngine.Object target, object from, object to)
    {
        // if you change something redo is cleared
        availableRedos.Clear();

        // Add this action do undoable actions
        availableUndos.Push(new UndoableAction(type, target, from, to));
    }

    public void Redo()
    {
        if (availableRedos.Count == 0) return;

        // get latest entry added to available Redos
        var redo = availableRedos.Pop();

        switch (redo.Type)
        {
            case UndoableActionType.Enable:
                Behaviour bhvr = (Behaviour)((Component)redo.Target);
                bhvr.enabled = (bool)redo.To;
                //((Component)redo.Target).enabled = (bool)redo.To;
                break;

            case UndoableActionType.SetActive:
                ((GameObject)redo.Target).SetActive((bool)redo.To);
                break;

            case UndoableActionType.Position:
                ((Transform)redo.Target).position = (Vector3)redo.To;
                break;

            case UndoableActionType.transform:

                break;

            case UndoableActionType.parenttransform:
                break;
                // ... According to your needs
        }

        // finally this is now a new undoable action
        availableUndos.Push(redo);
    }

    public void Undo()
    {
        if (availableUndos.Count == 0)
        {
            Gamplay_panel_script.I.set_undo_but(false);
            return;
        }
        // get latest entry added to available Undo
        var undo = availableUndos.Pop();

        switch (undo.Type)
        {
            case UndoableActionType.Enable:
                Behaviour bhvr = (Behaviour)((Component)undo.Target);
                bhvr.enabled = (bool)undo.To;
                //((Component)undo.Target).enabled = (bool)undo.From;
                break;

            case UndoableActionType.SetActive:
                ((GameObject)undo.Target).SetActive((bool)undo.From);
                break;

            case UndoableActionType.Position:
                ((Transform)undo.Target).position = (Vector3)undo.From;
                break;

            case UndoableActionType.transform:

                break;

            case UndoableActionType.parenttransform:
                //undo_move = true;
                ((GameObject)undo.Target).GetComponent<Card>().undoing_card = true;
                //((GameObject)undo.Target).transform.SetParent(null);
                if (Tb.I.hdcspot.Objects.Contains(((GameObject)undo.Target).GetComponent<Card>()))
                {
                    Tb.I.hdcspot.RemoveCard(((GameObject)undo.Target).GetComponent<Card>());
                }
                else
                {
                    Drag_drop_script.I.set_initial_size_card(((GameObject)undo.Target).GetComponent<Card>());
                }
                ((GameObject)undo.Target).GetComponent<Card>().MoveCard((Vector3)undo.From, true);
                //if(GameBoard.Instance.Automeld_bool)
                //{
                //    Card ss = ((GameObject)undo.Target).GetComponent<Card>();
                //    Tb.I.hdcspot.AddCard(ss);
                //}
                Debug.Log(((GameObject)undo.Target).GetComponent<Card>());

                //((Transform)undo.From);
                break;

            case UndoableActionType.automeld:
                //undo_move = true;
                //((GameObject)undo.Target).GetComponent<Card>().undoing_card = true;
                //((GameObject)undo.Target).transform.SetParent(null);
                //if (Tb.I.hdcspot.Objects.Contains(((GameObject)undo.Target).GetComponent<Card>()))
                //{
                //    Tb.I.hdcspot.RemoveCard(((GameObject)undo.Target).GetComponent<Card>());
                //}
                //else
                //{
                //    Drag_drop_script.I.set_initial_size_card(((GameObject)undo.Target).GetComponent<Card>());
                //}
                //((GameObject)undo.Target).GetComponent<Card>().MoveCard((Vector3)undo.From, true);
                //if(GameBoard.Instance.Automeld_bool)
                //{
                Card ss = ((GameObject)undo.Target).GetComponent<Card>();
                Tb.I.hdcspot.AddCard(ss);
                //}
                //Tb.I.hdcspot.AddCard();
                Debug.Log(((GameObject)undo.Target).GetComponent<Card>());

                //((Transform)undo.From);
                break;

                // ... According to your needs
        }

        // finally this is now a new  redoable action
        availableRedos.Push(undo);
    }

    public int Return_Available_Undo()
    {
        if(availableUndos.Count==0)
        {
            Gamplay_panel_script.I.set_undo_but(false);
            //Gamplay_panel_script.I.Undo.interactable = false;
            //Gamplay_panel_script.I.Undo.interactable = false;
        }
        return availableUndos.Count;
    }

    public void Undo_all()
    {
        int cc = availableUndos.Count;
        for (int i = 0; i < cc; i++)
        {
            //Debug.Log("here" + availableUndos.Count);
            Undo();
        }
    }

    public void Reset_Undo()
    {
        availableUndos.Clear();
    }
}
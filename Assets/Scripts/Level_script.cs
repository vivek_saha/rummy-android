﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_script : MonoBehaviour
{
    public static Level_script I;

    public int xp_counter
    {
        get
        {
            return PlayerPrefs.GetInt("xp_counter", 0);
        }
        set
        {

            PlayerPrefs.SetInt("xp_counter", value);
        }
    }


    private void Awake()
    {
        I = this;
    }


    public void add_xp()
    {
        xp_counter++;
        if(xp_counter== GameManager.Instance.Level_var*2)
        {
            GameManager.Instance.Level_var += 1;
            GameManager.Instance.set_lvlup_panel();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

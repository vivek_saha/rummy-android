﻿using rummy.Utility;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Firebase.Analytics;
using Firebase.Messaging;
using Firebase.Extensions;
using System;
using System.Threading.Tasks;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    //public Singleton<GameManager> single; = new Singleton<GameManager>;
    NativeShare ns;
    public GameObject Particle_Prefab;
    public GameObject lvl_up_panel;
    public Image gm_profile_pic;
    public string U_name;
    public GameObject nem_popup;
    public GameObject Black_bg;
    public Transform canvas_content;
    public RectTransform origin;

    private string topic = "RummyGameTopic";


    
    public int Coin_var
    {
        get
        {
            return PlayerPrefs.GetInt("Coin_value", 0);
        }
        set
        {
            PlayerPrefs.SetInt("Coin_value", value);
            Update_UIcoin_value();
        }
    }

    public int Level_var
    {
        get
        {
            return PlayerPrefs.GetInt("Level_value", 0);
        }
        set
        {
            PlayerPrefs.SetInt("Level_value", value);
            Update_UIlvl_value();
        }
    }

    public int App_ver_code;

    public int reward;
    public int fee;




    private void Awake()
    {

        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
            //DontDestroyOnLoad(this.gameObject);

        }
        //Instance = Singleton<GameManager>.Instance;
    }


    public void Update_UIcoin_value()
    {
        if (Homepanel_script.I != null)
        {
            Homepanel_script.I.wallet_coin.text = Coin_var.ToString();
            Homepanel_script.I.wallet_coin_profilepanel.text = Coin_var.ToString();
        }
    }

    public void Update_UIlvl_value()
    {
        if (Homepanel_script.I != null)
        {
            Homepanel_script.I.LVL.text = Level_var.ToString();
            Homepanel_script.I.LVLV_COPY.text = Level_var.ToString();
        }
    }


    // Start is called before the first frame update
    private void Start()
    {
        ns = new NativeShare();
        FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
        //set_script.set_toogles();
        crashanalytics_init();
    }

    #region Firebase
    public void crashanalytics_init()
    {
        // Initialize Firebase
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                // Crashlytics will use the DefaultInstance, as well;
                // this ensures that Crashlytics is initialized.
                Firebase.FirebaseApp app = Firebase.FirebaseApp.DefaultInstance;
                InitializeFirebaseMessaging();
                // Set a flag here for indicating that your project is ready to use Firebase.
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
    }


    void InitializeFirebaseMessaging()
    {
        //FirebaseMessaging.MessageReceived += OnMessageReceived;
        //FirebaseMessaging.TokenReceived += OnTokenReceived;
        FirebaseMessaging.SubscribeAsync(topic).ContinueWithOnMainThread(task =>
        {
            LogTaskCompletion(task, "SubscribeAsync");
        });
        Debug.Log("Firebase Messaging Initialized");

        // This will display the prompt to request permission to receive
        // notifications if the prompt has not already been displayed before. (If
        // the user already responded to the prompt, thier decision is cached by
        // the OS and can be changed in the OS settings).
        FirebaseMessaging.RequestPermissionAsync().ContinueWithOnMainThread(
          task =>
          {
              LogTaskCompletion(task, "RequestPermissionAsync");
          }
        );
        //isFirebaseInitialized = true;
    }

    private bool LogTaskCompletion(Task task, string operation)
    {
        bool complete = false;
        if (task.IsCanceled)
        {
            Debug.Log(operation + " canceled.");
        }
        else if (task.IsFaulted)
        {
            Debug.Log(operation + " encounted an error.");
            foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
            {
                string errorCode = "";
                Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
                if (firebaseEx != null)
                {
                    errorCode = String.Format("Error.{0}: ",
                      ((Firebase.Messaging.Error)firebaseEx.ErrorCode).ToString());
                }
                Debug.Log(errorCode + exception.ToString());
            }
        }
        else if (task.IsCompleted)
        {
            Debug.Log(operation + " completed");
            complete = true;
        }
        return complete;
    }

    #endregion






    public void start_game_again()
    {
        Time.timeScale = 1;
        Splash_Panel_scripts.I.custom_start();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject a = Instantiate(Particle_Prefab);
            Vector3 temmp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            temmp.z = -5;
            a.transform.position = temmp;
        }
    }

    public void Share()
    {

#if UNITY_ANDROID
        ns.SetText(Setting_API.Instance.fb_share_link_title + "\n" + Setting_API.Instance.android_app_link).Share();
#elif UNITY_IOS
        ns.SetText(Setting_API.Instance.fb_share_link_title+"\n"+Setting_API.Instance.ios_app_link).Share();
#endif

    }



    public void set_lvlup_panel()
    {
        lvl_up_panel.GetComponent<LvlUP_panel_script>().Coins_value.text = (500 * Level_var).ToString();
        lvl_up_panel.GetComponent<LvlUP_panel_script>().level_text.text = (Level_var).ToString();
        lvl_up_panel.GetComponent<LvlUP_panel_script>().Particle_spark.SetActive(true);
        lvl_up_panel.SetActive(true);
    }
    public void collectlvl_reward()
    {
        Coin_var += int.Parse(lvl_up_panel.GetComponent<LvlUP_panel_script>().Coins_value.text);
        Uploaddata_server();
        lvl_up_panel.GetComponent<LvlUP_panel_script>().Particle_spark.SetActive(false);
        lvl_up_panel.SetActive(false);
        Homepanel_script.I.Update_lvl_lockes();
    }

    public void Uploaddata_server()
    {
        Update_API.Instance.is_update = 1;
        Update_API.Instance.type = "lvl_up_collect";
        Update_API.Instance.credits = Coin_var;
        Update_API.Instance.xp = Level_var;
        Update_API.Instance.Update_data();
    }

    public void OnStart_lvl()
    {


        set_gameplay_panel();
        Scene_shred_script.Instance.OnStart_lvl();
        //SceneManager.LoadScene("Gameplay");

    }


    public void button_click_sound()
    {
        Audiomanager.Instance.button_click_sound();

    }

    public bool Check_coins(int x)
    {
        if (Coin_var >= x)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void set_gameplay_panel()
    {
        Scene_shred_script.Instance.temp_profile_pic = gm_profile_pic.sprite;
        Scene_shred_script.Instance.U_name = U_name;
        Scene_shred_script.Instance.fee = fee;
        Scene_shred_script.Instance.reward = reward;
        //Scene_shred_script.Instance.
    }

    public void nem_popup_fun(string msg)
    {
        GameObject a = Instantiate(nem_popup);
        a.GetComponent<nem_popup_script>().msg.text = msg;
        a.transform.SetParent(canvas_content);
        a.GetComponent<RectTransform>().localScale = Vector3.one;
        a.transform.position = origin.transform.position;
    }

    public void Onpurchase_complete(int amount)
    {
        //PlayerPrefs.SetString("Slot_credits", (long.Parse(PlayerPrefs.GetString("Slot_credits")) + amount).ToString());
        //if (startup_purchase_card_panel.activeSelf)
        //{
        //    startup_purchase_card_panel.SetActive(false);
        //}
        //temp_amount = amount;
        //Setting_vlaues();
        //if (Gameplay_panel.activeSelf)
        //{
        //    Lvl_manager.Instance.OnPurchase_coins(amount);
        //}
        Coin_var += amount;
        Shop_panel_script.Instance.shoppanel.SetActive(false);
        if (PlayerPrefs.GetString("Token", string.Empty) != string.Empty)
        {
            Update_API.Instance.is_update = 1;
            Update_API.Instance.type = "purchase";
            Update_API.Instance.Update_data();
        }
    }

    public void Privacy_policy()
    {

        Application.OpenURL(Setting_API.Instance.privacy_policy);


    }

    public void SendEmail()
    {
        string email = "sahajanandinfotech1@gmail.com";
        string subject = MyEscapeURL("");
        string body = MyEscapeURL("");
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }
    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }

}
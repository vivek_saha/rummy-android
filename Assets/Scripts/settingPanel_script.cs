﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class settingPanel_script : MonoBehaviour
{

    public static settingPanel_script I;

   
    public Toggle sound, music, vibrate;
   
    public GameObject sound_off, sound_on, Music_off, Music_on, vibrate_off, vibrate_on;

    


    private void Awake()
    {
        I = this;
        DontDestroyOnLoad(this.gameObject);
    }


    public void setToggles()
    {
        sound_on.SetActive(Audiomanager.Instance.Sound_bool);
        sound_off.SetActive(!Audiomanager.Instance.Sound_bool);
        Music_on.SetActive(Audiomanager.Instance.Music_bool);
        Music_off.SetActive(!Audiomanager.Instance.Music_bool);
        vibrate_on.SetActive(Audiomanager.Instance.Vibrate_bool);
        vibrate_off.SetActive(!Audiomanager.Instance.Vibrate_bool);
        sound.isOn = Audiomanager.Instance.Sound_bool;
        music.isOn = Audiomanager.Instance.Music_bool;
        vibrate.isOn = Audiomanager.Instance.Vibrate_bool;
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void toogle_sound()
    {
        //Debug.Log(sound.isOn);
        Audiomanager.Instance.Sound_bool = sound.isOn;
        sound_on.SetActive(Audiomanager.Instance.Sound_bool);
        sound_off.SetActive(!Audiomanager.Instance.Sound_bool);
    }
    public void toogle_music()
    {
        //Debug.Log(music.isOn);
        Audiomanager.Instance.Music_bool = music.isOn;
        Music_on.SetActive(Audiomanager.Instance.Music_bool);
        Music_off.SetActive(!Audiomanager.Instance.Music_bool);
    }
    public void toogle_vibrate()
    {
        //Debug.Log(vibrate.isOn);
        Audiomanager.Instance.Vibrate_bool = vibrate.isOn;
        vibrate_on.SetActive(Audiomanager.Instance.Vibrate_bool);
        vibrate_off.SetActive(!Audiomanager.Instance.Vibrate_bool);
    }
}

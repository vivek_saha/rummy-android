﻿using UnityEngine;

public class Undo_Script : MonoBehaviour
{
    public static Undo_Script i;

    private void Awake()
    {
        i = this;
    }

    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void Undo()
    {
        Debug.Log(UndoRedoSystem.I.Return_Available_Undo());
        if (UndoRedoSystem.I.Return_Available_Undo() > 0)
        {
            UndoRedoSystem.I.Undo();
            //if(GameBoard.Instance.Automeld_bool)
            //{
            //}
        }
        else
        {
            Gamplay_panel_script.I.set_undo_but(false);
        }
        GameBoard.Instance.refresh_gameboard();
    }

    public void UndoAll()
    {
        if (UndoRedoSystem.I.Return_Available_Undo() > 0)
        {
            UndoRedoSystem.I.Undo_all();
            Gamplay_panel_script.I.set_undo_but(false);
            GameBoard.Instance.refresh_gameboard();
        }
    }
}
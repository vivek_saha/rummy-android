﻿using System.Collections;

using System.Collections.Generic;

using UnityEngine;

using Facebook.Unity;

using UnityEngine.UI;

using System;



public class fblogin : MonoBehaviour

{

    public static fblogin I;

    // public GameObject Panel_Add;

    //public Text FB_userName;

    //public Image FB_useerDp;




    public GameObject fb_but;
    //public GameObject friendstxtprefab;

    //public GameObject GetFriendsPos;

    private static readonly string EVENT_PARAM_SCORE = "score";

    private static readonly string EVENT_NAME_GAME_PLAYED = "game_played";

    private void Awake()

    {
        I = this;
        // FB.Init(SetInit, onHidenUnity);

        // Panel_Add.SetActive(false);





        if (!FB.IsInitialized)

        {

            FB.Init(() =>

            {

                if (FB.IsInitialized)

                    FB.ActivateApp();

                else

                    Debug.LogError("Couldn't initialize");

            },

            isGameShown =>

            {

                if (!isGameShown)

                    Time.timeScale = 0;

                else

                    Time.timeScale = 1;

            });

        }

        else

            FB.ActivateApp();

    }

    void SetInit()

    {

        if (FB.IsLoggedIn)

        {

            Debug.Log("Facebook is Login!");

        }

        else

        {

            Debug.Log("Facebook is not Logged in!");

        }

        DealWithFbMenus(FB.IsLoggedIn);

    }



    void onHidenUnity(bool isGameShown)

    {

        if (!isGameShown)

        {

            Time.timeScale = 0;

        }

        else

        {

            Time.timeScale = 1;

        }

    }

    public void FBLogin()

    {

        List<string> permissions = new List<string>();

        permissions.Add("public_profile");
        permissions.Add("email");
        //permissions.Add("user_friends");



        FB.LogInWithReadPermissions(permissions, AuthCallBack);

    }



    public void CallLogout()

    {

        StartCoroutine("FBLogout");

    }

    IEnumerator FBLogout()

    {

        FB.LogOut();

        while (FB.IsLoggedIn)

        {

            print("Logging Out");

            yield return null;

        }

        print("Logout Successful");

        //FB_useerDp.sprite = null;

        //FB_userName.text = "";

    }





    
    public void FacebookSharefeed()

    {

        string url = "https://developers.facebook.com/docs/unity/reference/current/FB.ShareLink";

        FB.ShareLink(

            new Uri(url),

            "Checkout unity3d teacher channel",

            "I just watched " + "22" + " times of this channel",

            null,

            ShareCallback);



    }

    private static void ShareCallback(IShareResult result)

    {

        Debug.Log("ShareCallback");

        SpentCoins(2, "sharelink");

        if (result.Error != null)

        {

            Debug.LogError(result.Error);

            return;

        }

        Debug.Log(result.RawResult);

    }

    // Start is called before the first frame update

    void AuthCallBack(IResult result)

    {

        if (result.Error != null)

        {

            Debug.Log(result.Error);

        }

        else

        {

            if (FB.IsLoggedIn)

            {

                Debug.Log("Facebook is Login!");

                // Panel_Add.SetActive(true);

            }

            else

            {

                Debug.Log("Facebook is not Logged in!");

            }

            DealWithFbMenus(FB.IsLoggedIn);

        }

    }



    void DealWithFbMenus(bool isLoggedIn)

    {

        if (isLoggedIn)

        {
            //Debug.Log();
            Login_Api_script.I.login_type = "facebook";
            Login_Api_script.I.fb_id = FB.Mobile.UserID;
            Login_Api_script.I.wallet_coin = 0;
            Login_Api_script.I.current_xp = 0;
            PlayerPrefs.SetInt("facebook", 1);
            //Debug.Log(FB.Mobile.UserID);
            FB.API("/me?fields=first_name", HttpMethod.GET, DisplayUsername);
            FB.API("/me?fields=id", HttpMethod.GET, Displayfb_id);
            FB.API("/me?fields=email", HttpMethod.GET, Displayemail);

            FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET, DisplayProfilePic);
            fb_but.SetActive(false);
           
            Splash_Panel_scripts.I.Login_panel.SetActive(false);
            Splash_Panel_scripts.I.bgimage.SetActive(true);
            Notice_panel_script.Instance.notice_Panel.SetActive(true);
            Notice_panel_script.Instance.Loading_panel.SetActive(true);

        }

        else

        {



        }

    }

    void DisplayUsername(IResult result)

    {

        if (result.Error == null)

        {

            string asdkj = "" + result.ResultDictionary["first_name"];
            Login_Api_script.I.Name = asdkj;
            //FB_userName.text = name;
            //result.ResultDictionary[]


            Debug.Log("" + asdkj);

        }

        else

        {

            Debug.Log(result.Error);

        }

    }
    void Displayfb_id(IResult result)

    {

        if (result.Error == null)

        {

            string abc = "" + result.ResultDictionary["id"];
            Login_Api_script.I.fb_id = abc;
            //FB_userName.text = name;
            //result.ResultDictionary[]


            Debug.Log("" + abc);

        }

        else

        {

            Debug.Log(result.Error);

        }

    }

    void Displayemail(IResult result)

    {

        if (result.Error == null)

        {

            string email_1 = "" + result.ResultDictionary["email"];
            Login_Api_script.I.email = email_1;
            //FB_userName.text = name;
            //result.ResultDictionary[]


            Debug.Log("" + email_1);

        }

        else

        {

            Debug.Log(result.Error);

        }

    }



    void DisplayProfilePic(IGraphResult result)

    {

        if (result.Texture != null)

        {

            Debug.Log("Profile Pic");
            edit_profile_panel.I.Save_image_to_file(result.Texture);
            //FB_useerDp.sprite = Sprite.Create(result.Texture, new Rect(0, 0, 128, 128), new Vector2());
            Login_Api_script.I.Start_upload();
        }

        else

        {

            Debug.Log(result.Error);
            Login_Api_script.I.Start_upload();

        }

    }

    public static void SpentCoins(int coins, string item)
    {

        // setup parameters

        var param = new Dictionary<string, object>();

        param[AppEventParameterName.ContentID] = item;

        // log event

        FB.LogAppEvent(AppEventName.SpentCredits, (float)coins, param);

    }



}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class guest_login : MonoBehaviour
{
    public static guest_login I;


    public string U_name;



    private void Awake()
    {
        I = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Onlogin_guest()
    {
        //if(PlayerPrefs.SetInt("GuestLogin"))
        //PlayerPrefs.SetInt("logged in", 1);
        Splash_Panel_scripts.I.Login_panel.SetActive(false);
        Splash_Panel_scripts.I.bgimage.SetActive(true);
        Login_Api_script.I.login_type = "guest";
        Login_Api_script.I.Name = U_name;
        Login_Api_script.I.wallet_coin = 0;
        Login_Api_script.I.current_xp = 0;
        PlayerPrefs.SetInt("GuestLogin", 1);
        //Save_image_to_file(temp_tex);
        Login_Api_script.I.Start_upload();
        Notice_panel_script.Instance.notice_Panel.SetActive(true);
        Notice_panel_script.Instance.Loading_panel.SetActive(true);
        //Splash_Panel_scripts.Instance.Loading_screen();
        //PlayerPrefs.SetInt("logged in", 1);
    }
}

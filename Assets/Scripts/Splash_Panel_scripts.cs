﻿using UnityEngine;

public class Splash_Panel_scripts : MonoBehaviour
{
    public static Splash_Panel_scripts I;

    //public GameObject
    public GameObject Login_panel;
    public GameObject Splash_panel;
    public GameObject bgimage;
    private void Awake()
    {
        I = this;
    }

    public void custom_start()
    {
        Invoke("turnoff_splash_screen", 4f);
    }
    // Start is called before the first frame update
    private void Start()
    {
        Invoke("turnoff_splash_screen", 4f);
    }

    public void turnoff_splash_screen()
    {
        Splash_panel.SetActive(false);
        TurnOn_login_panel();
    }

    public void TurnOn_login_panel()
    {
        if (PlayerPrefs.GetString("Token",string.Empty) == string.Empty)
        {
            //not login
            Login_panel.SetActive(true);
        }
        else
        {
            //login
            Setting_API.Instance.Get_settings_data();
            bgimage.SetActive(true);
            
            Notice_panel_script.Instance.notice_Panel.SetActive(true);
            Notice_panel_script.Instance.Loading_panel.SetActive(true);
        }


    }

    // Update is called once per frame
    private void Update()
    {
    }
}
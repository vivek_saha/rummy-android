﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using rummy;

public class Gamplay_panel_script : MonoBehaviour
{

    public static Gamplay_panel_script I;

    public GameObject gamemaster,fb_obj;
    public GameObject Loading_Panel;
    public Image[] player_profile_pic;
    public Button Draw, Undo, Undo_all, Arrange, Automeld;
    public Image[] players;
    public Sprite activeuser, deactiveuser;
    public Text[] Usernames;

    public GameObject nem_popup;

    public Transform canvas_content;
    public RectTransform origin;


    public GameObject Black_bg;
    private void Awake()
    {
        I = this;
    }
    // Start is called before the first frame update
    void Start()
    {
       
        Invoke("Disable_loading_panel", 2.5f);
    }

    private void OnEnable()
    {
        Scene_shred_script.Instance.set_gameplay_panel();
    }

    public void Disable_loading_panel()
    {
        if (PlayerPrefs.GetInt("facebook")==1)
        {
            fb_obj.SetActive(false);

        }
        Loading_Panel.SetActive(false);
        Ads_priority_script.Instance.Show_interrestial();
        //nem_popup_fun("test");
        gamemaster.GetComponent<GameMaster>().gamestarted = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Change_Active_player(int a)
    {
        for (int i = 0; i < 4; i++)
        {
            if (i == a)
            {
                players[i].sprite = activeuser;
                players[i].rectTransform.sizeDelta = new Vector2(405,105);
            }
            else
            {
                players[i].sprite = deactiveuser;
                players[i].rectTransform.sizeDelta = new Vector2(380, 105);
            }
        }
    }
    public void Disable_player_buttons()
    {
        Draw.interactable = false;
        Undo.interactable = false;
        Undo_all.interactable = false;
        Automeld.interactable = false;
    }


    public void set_undo_but(bool t)
    {
        Undo.interactable = t;
        Undo_all.interactable = t;
    }
    public void Enable_player_buttons()
    {
        Draw.interactable = true;
        //Undo.interactable = true;
        //Undo_all.interactable = true;
        Automeld.interactable = true;
    }

    public void BackToLobby()
    {
        //GameManager.Instance.Coin_var -= GameManager.Instance.fee;
        Time.timeScale = 1;
        Loading_Panel.SetActive(true);
        Scene_shred_script.Instance.Uploaddata_server();
       
    }
    public void BackToLobby_not_finished()
    {
        Time.timeScale = 1;
        Loading_Panel.SetActive(true);
        int a = PlayerPrefs.GetInt("Coin_value", 0) - Scene_shred_script.Instance.fee;
        PlayerPrefs.SetInt("Coin_value", a);
        //GameManager.Instance.Coin_var -= GameManager.Instance.fee;
        Scene_shred_script.Instance.Uploaddata_server();
        
    }

    public void Playagain()
    {
        Time.timeScale = 1;
        Loading_Panel.SetActive(true);
        Update_API.Instance.playagain = true;
        Scene_shred_script.Instance.Uploaddata_server();
        SceneManager.LoadScene("Gameplay");
    }

    IEnumerator aftersceneload()
    {
        yield return SceneManager.LoadSceneAsync("Gameplay");
        Time.timeScale = 1;
    }


    public void button_click_sound()
    {
        Audiomanager.Instance.button_click_sound();
    }


    public void nem_popup_fun(string msg)
    {
        
        GameObject a = Instantiate(nem_popup);
        a.GetComponent<nem_popup_script>().msg.text = msg;
        a.transform.SetParent(canvas_content);
        a.GetComponent<RectTransform>().localScale = Vector3.one;
        a.transform.position = origin.transform.position;
    }
}

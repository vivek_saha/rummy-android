﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audiomanager : MonoBehaviour
{

    public static Audiomanager Instance;

    public AudioSource BG_Music, Coin_throw_sound, Monster_hit_sound, slot_sound, Button_click;

    public GameObject coin_fall_prefab;
    //public AudioClip coin_throw_sound;

    public bool Sound_bool
    {
        get
        {
            if (PlayerPrefs.GetString("Sound_bool", "True") == "True")
            {
                eneble_sounds();
                return true;
            }
            else
            {
                disable_sounds();
                return false;
            }
        }
        set
        {
            //Debug.Log(value);
            if (value)
            {
                //eneble_sounds();
                PlayerPrefs.SetString("Sound_bool", value.ToString());
            }
            else
            {
                //disable_sounds();
                PlayerPrefs.SetString("Sound_bool", value.ToString());
            }
            //Setting_panel_script.Instance.set_toogles();
        }
    }


    public bool Music_bool
    {
        get
        {
            if (PlayerPrefs.GetString("Music_bool", "True") == "True")
            {
                //eneble_sounds();
                BG_Music.enabled = true;
                return true;
            }
            else
            {
                //disable_sounds();
                BG_Music.enabled = false;
                return false;
            }
        }
        set
        {
            //Debug.Log(value);
            if (value)
            {
                //eneble_sounds();
                BG_Music.enabled = true;
                PlayerPrefs.SetString("Music_bool", value.ToString());
            }
            else
            {
                //disable_sounds();
                BG_Music.enabled = false;
                PlayerPrefs.SetString("Music_bool", value.ToString());
            }
            //Setting_panel_script.Instance.set_toogles();
        }
    }
    public bool Vibrate_bool
    {
        get
        {
            if (PlayerPrefs.GetString("Vibrate_bool", "True") == "True")
            {
                //eneble_sounds();
                return true;
            }
            else
            {
                //disable_sounds();
                return false;
            }
        }
        set
        {
            if (value)
            {
                //eneble_sounds();
                PlayerPrefs.SetString("Vibrate_bool", value.ToString());
            }
            else
            {
                //disable_sounds();
                PlayerPrefs.SetString("Vibrate_bool", value.ToString());
            }
            //Setting_panel_script.Instance.set_toogles();
        }
    }
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }


        Instance = this;
        DontDestroyOnLoad(this.gameObject);


    }
    // Start is called before the first frame update
    void Start()
    {

        Sound_bool = Sound_bool;
        Vibrate_bool = Vibrate_bool;

        settingPanel_script.I.setToggles();
        //GameManager.Instance.set_toogles();
        //Debug.Log("sounds:" + Sound_bool);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void eneble_sounds()
    {
        //Coin_throw_sound.enabled = true;
        //Monster_hit_sound.enabled = true;
        //slot_sound.enabled = true;
        //Coin_fall.enabled = true;
        Button_click.enabled = true;
    }


    public void disable_sounds()
    {



        //Coin_throw_sound.enabled = false;
        //Monster_hit_sound.enabled = false;
        //slot_sound.enabled = false;
        //Coin_fall.enabled = false;
        Button_click.enabled = false;
    }


    public void button_click_sound()
    {
        Button_click.Play();
    }

    public void coin_fall_sounds()
    {
        Instantiate(coin_fall_prefab);
    }

    //private static readonly AndroidJavaObject Vibrator =
    //new AndroidJavaClass("com.unity3d.player.UnityPlayer")// Get the Unity Player.
    //.GetStatic<AndroidJavaObject>("currentActivity")// Get the Current Activity from the Unity Player.
    //.Call<AndroidJavaObject>("getSystemService", "vibrator");// Then get the Vibration Service from the Current Activity.

    //static void KyVibrator()
    //{
    //    // Trick Unity into giving the App vibration permission when it builds.
    //    // This check will always be false, but the compiler doesn't know that.
    //    if (Application.isEditor) Handheld.Vibrate();
    //}

    //public static void Vibrate(long milliseconds)
    //{
    //    Vibrator.Call("vibrate", milliseconds);
    //}

    //public static void Vibrate(long[] pattern, int repeat)
    //{
    //    Vibrator.Call("vibrate", pattern, repeat);
    //}
}

﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LeaderBoard_API : MonoBehaviour
{
    //public Start_lederboard_panel sc;
    public GameObject Leaderboard_prefab, First, Second, Third, Player,leaderboard_loading;
    public Transform leaderboard_content;

    public JSONNode jsonResult;


    string u_name, code;
    // create the web request and download handler
    //UnityWebRequest webReq = new UnityWebRequest();

    string url = "http://104.131.36.193:6800/api/leaderboard";
    string rawJson;
    //string token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImE0YzFhOWZkNjQ4ZmZkYmQzNjVkMTY3M2UwOWQ5MjE3MDI5YTM1OTgwMTRlY2Y4NmZlZGQ1ODhkNzE1NWZjYjg1OTZkYzVlYTEyYWZjYmQxIn0.eyJhdWQiOiIxIiwianRpIjoiYTRjMWE5ZmQ2NDhmZmRiZDM2NWQxNjczZTA5ZDkyMTcwMjlhMzU5ODAxNGVjZjg2ZmVkZDU4OGQ3MTU1ZmNiODU5NmRjNWVhMTJhZmNiZDEiLCJpYXQiOjE2MjI4MDA5NTgsIm5iZiI6MTYyMjgwMDk1OCwiZXhwIjoxNjU0MzM2OTU4LCJzdWIiOiIxMTc2Iiwic2NvcGVzIjpbXX0.UZ1hLriE29jdsfhWDHooXrFONYGf4pqboKS4Ren-Fz_fOK7HTc3bl2zlm0As3Fft1mBloWMyFkPgue0Z83Vt9mZZjsIVbfjwGqocGCVYGw1xPMGHkFLbqkKa8kMPpIJrS0zXMLvsYRTDbBP91BSBPir96fDH2G-SC67qj5t4NAiDPsYoAfVbW3QSjcxDZIgrVqi5xnrMUdv8HU-5FM6MHYmqhfnuTVLkpylxmK4e6ak1O_HeUAa20w2xy8D5EgeVKDdA3z2p_32R3UM1JxkJmbn7AnDs4Ism0sQpG2advCaz5zJgw3Wfw2xsRPTk3FWzRV8mf76JrKkbfZQXdm_W9x9BS6cSsRgQaKlsC7n0ouHnn22kM3WNe_g7v9Yr3QipcUrHiRc-6wmtPgU3j9-hray49oTpnAHyH9b9tMYbC3e309NrdM9ZO0n5NISEquIDDdiTWU6aZdwiRauZ_ED3KJoZf4-iLSPcWborhnBudbWb3OXrbp7ZjmoWpAAyXYwKR_Cr-ws0Scue-vU8AG4qLKIF5LqNUz9ELWc4KhPWRXLUYZueLqhNn62LBD8R3yyN2y6_fUOY9xRg43lWVYFXexXfCaAQlQbVCitrVJ_I05vR-GWbuZP1vWcVf5TeQ_xRsE9q7q_PZ_gYvzv-XsHf6p3y0c_zYDv-ArKDKbVE9yg";
    // Start is called before the first frame update
    public void Custom_Start()
    {

        StartCoroutine("GetData");
    }

    public void Onleaderboard_click()
    {
        //sc.Manual_Start();
        if (leaderboard_content.childCount > 0)
        {
            for (int i = 0; i < leaderboard_content.childCount; i++)
            {
                Destroy(leaderboard_content.GetChild(i).gameObject);
            }

        }
        StartCoroutine("GetData");
    }

    IEnumerator GetData()
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        //req_call = false;
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        yield return www.Send();
        Debug.Log(www.responseCode);
        if (www.isNetworkError)
        {
            Debug.Log(www.error);


            Check_Internet_connection();
            //req_call = true;
        }
        else
        {
            close_Retry_panel();
            // Show results as text
            Debug.Log(www.downloadHandler.text);
            rawJson = www.downloadHandler.text;
            //Manual_Start();
            Set_Json_data();
            // Or retrieve results as binary data
            // byte[] results = www.downloadHandler.data;
        }
    }

    public void Set_Json_data()
    {
        GameObject temp;
        jsonResult = JSON.Parse(rawJson);

        foreach(Transform a in leaderboard_content.transform )
        {
            Destroy(a.gameObject);
        }


        int counter = 1;
        foreach (JSONNode data in jsonResult["data"]["toppers"])
        {
            if (counter < 4)
            {
                if(counter == 1)
                {
                    Set_Toppers(First, data["name"].Value, data["profile_picture"].Value, data["wallet_coin"].AsFloat);
                }
                else if(counter == 2)
                {
                    Set_Toppers(Second, data["name"].Value, data["profile_picture"].Value, data["wallet_coin"].AsFloat);
                }
                else if(counter == 3)
                {
                    Set_Toppers(Third, data["name"].Value, data["profile_picture"].Value, data["wallet_coin"].AsFloat);
                }
            }
            else
            {
                temp = Instantiate(Leaderboard_prefab, Vector3.zero, Quaternion.identity);
                temp.transform.parent = leaderboard_content.transform;
                temp.transform.localScale = new Vector3(1, 1, 1);
                temp.GetComponent<LeaderBoard_container_script>().Number.text = counter.ToString();
                temp.GetComponent<LeaderBoard_container_script>().profile_pic_link_2 = data["profile_picture"].Value;
                Set_username(data["name"].Value);
                temp.GetComponent<LeaderBoard_container_script>().Name.text = data["name"].Value;
                //temp.GetComponent<LeaderBoard_container_script>().code.text = code;
                //temp.GetComponent<LeaderBoard_container_script>().lvl_text.text = (computeLevel(data["current_xp"].AsInt)).ToString();
                temp.GetComponent<LeaderBoard_container_script>().coin.text = AbbrevationUtility_LeaderBoard.AbbreviateNumber(data["wallet_coin"].AsFloat);
                temp = null;
            }
            counter++;
        }
        Player.GetComponent<LeaderBoard_container_script>().Number.text = jsonResult["data"]["user"]["rank"].Value;
        Player.GetComponent<LeaderBoard_container_script>().profile_pic_link_2 = jsonResult["data"]["user"]["profile_picture"].Value;
        Player.GetComponent<LeaderBoard_container_script>().Name.text = jsonResult["data"]["user"]["name"].Value;
        Player.GetComponent<LeaderBoard_container_script>().coin.text = jsonResult["data"]["user"]["wallet_coin"].Value;
        Player.GetComponent<LeaderBoard_container_script>().custom_get_image();


       //te.GetComponent<LeaderBoard_container_script>()



       leaderboard_loading.SetActive(false);

    }


    public void Set_Toppers(GameObject te, string u, string l, float c)
    {
        te.GetComponent<LeaderBoard_container_script>().Name.text= u;
        te.GetComponent<LeaderBoard_container_script>().profile_pic_link_2 = l;
        te.GetComponent<LeaderBoard_container_script>().coin.text = AbbrevationUtility_LeaderBoard.AbbreviateNumber(c);
        te.GetComponent<LeaderBoard_container_script>().custom_get_image();
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void Set_username(string Username_server)
    {
        string rev = Reverse(Username_server);
        string[] splitArray = rev.Split(char.Parse("_"));
        code = Reverse(splitArray[0]);
        code = "_" + code;
        u_name = string.Empty;
        for (int i = 1; i < splitArray.Length; i++)
        {
            if (i == 1)
            {
                u_name += splitArray[i];
            }
            else
            {
                u_name += "_" + splitArray[i];
            }
        }
        u_name = Reverse(u_name);


        //username.text = u_name + "<size= 25><#828282>" + code + "</color></size>";
        //PlayerPrefs.SetString("", Send_Data_to_SErver.Instance.email);
    }
    public static string Reverse(string s)
    {
        char[] charArray = s.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);
    }
    //int computeLevel(int currentXP)
    //{
    //    float lvl = Mathf.Sqrt(currentXP / Lvl_manager.Instance.leveling.xpBase);



    //    //print("lvl" + lvl);
    //    int clevel = Mathf.FloorToInt(lvl + 1);
    //    //lvl_up_check(clevel);
    //    if (clevel == 0) clevel = 1;
    //    return clevel;
    //}
    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    Notice_panel_script.Instance.notice_Panel.SetActive(true);
                    Notice_panel_script.Instance.Something_wentwrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    Notice_panel_script.Instance.notice_Panel.SetActive(true);
                    Notice_panel_script.Instance.No_internet_Panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.No_internet_Panel.SetActive(true);
                }
            }
        }));
    }

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }
    //public bool check_net()
    //{
    //    WWW www = new WWW("http://www.google.com");
    //    //yield return www;
    //    if (www.error != null)
    //    {
    //        return false;
    //    }
    //    else
    //    {
    //        return true;
    //    }

    //}
    public void Retry_connection()
    {
        StartCoroutine("GetData");
        Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(false);
        Notice_panel_script.Instance.No_internet_Panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSeconds(3f);
        //StartCoroutine("check_connection");
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        Notice_panel_script.Instance.No_internet_Panel.SetActive(false);
        StartCoroutine("GetData");
    }
    public void close_Retry_panel()
    {
        Notice_panel_script.Instance.Something_wentwrong_panel.SetActive(false);
        Notice_panel_script.Instance.No_internet_Panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        Notice_panel_script.Instance.notice_Panel.SetActive(false);
    }
}

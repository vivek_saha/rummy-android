﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.Networking;

public class IAP_API : MonoBehaviour
{
    public static IAP_API Instance;
    string url = "http://104.131.36.193:6800/api/iap";
    public JSONNode jsonResult;

    //public Shop_panel_script sh_panel;

    [HideInInspector]
    public long[] first_val;
    [HideInInspector]
    public int[] percentage_val;
    [HideInInspector]
    public string[] price_val;
    [HideInInspector]
    public string[] product_id;

    public long St_purchase_first_val;
    public int St_purchase_percentage_val;
    public string St_purchase_price_val;
    public string St_purchase_product_id;
    // create the web request and download handler
    //UnityWebRequest webReq = new UnityWebRequest();

    GameObject temp_combo;

    string rawJson;
    // Start is called before the first frame update

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }

        else
        {
            Instance = this;
            //DontDestroyOnLoad(this.gameObject);

        }
    }
    void Start()
    {
        //if()
        //Get_settings_data();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Get_settings_data()
    {
        //Debug.Log("getdata");
        StartCoroutine("GetData");

    }

    IEnumerator GetData()
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        //req_call = false;
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        //token = PlayerPrefs.GetString("Token");
        yield return www.Send();
        //Debug.Log(www);
        if (www.isNetworkError)
        {
            Debug.Log(www.error);
            Get_settings_data();

            //Check_Internet_connection();
            //req_call = true;
        }
        else
        {
            //close_Retry_panel();
            // Show results as text
            //Debug.Log(www.downloadHandler.text);
            rawJson = www.downloadHandler.text;
            jsonResult = JSON.Parse(rawJson);
            Debug.Log("iap:"+rawJson);
            Set_Json_data();
            //Manual_Start();
            // Or retrieve results as binary data
            // byte[] results = www.downloadHandler.data;
        }
    }
    public void Set_Json_data()
    {
#if UNITY_IOS
string ad ="ios_";
#else
        string ad = "";
#endif
        int m = 0;
        foreach (JSONNode message in jsonResult["data"][ad + "purchase_coin"])
        {
            m++;
        }

        //Debug.Log("m" + m);
        first_val = new long[m];
        percentage_val = new int[m];
        price_val = new string[m];
        product_id = new string[m];


        int counter = 0;
        foreach (JSONNode message in jsonResult["data"][ad + "purchase_coin"])
        {

            first_val[counter] = message["coin"].AsLong;
            percentage_val[counter] = message["inr"].AsInt;
            price_val[counter] = message["is_video"].Value;
            product_id[counter] = message["product_id"].Value;

            counter++;
        }


        foreach (JSONNode message in jsonResult["data"][ad + "offer_coin"])
        {
            St_purchase_first_val = message["coin"].AsLong;
            St_purchase_percentage_val = message["inr"].AsInt;
            St_purchase_price_val = message["is_video"].Value;
            St_purchase_product_id = message["product_id"].Value;
            //Debug.Log(message["product_id"].Value);

        }
        IAPManager.I.manual_start();
        Generate_shop_panel();
        on_startup_purchase_card();
        //Debug.Log(Get_API_Data_IAP.Instance.St_purchase_product_id);
        //if (Get_API_Data_IAP.Instance.St_purchase_product_id != null)
        //{
        //    Get_API_Data_IAP.Instance.startup_purchase_card();
        //}
    }

    public void Generate_shop_panel()
    {
        if (Shop_panel_script.Instance.Scroll_content.transform.childCount > 2)
        {
            foreach (Transform a in Shop_panel_script.Instance.Scroll_content.transform)
            {
                if (a.transform.GetSiblingIndex() > 1)
                {
                    DestroyImmediate(a);
                }
            }
        }
        //Debug.Log("Get_API_Data_IAP.Instance.product_id.Length" + Get_API_Data_IAP.Instance.product_id.Length);
        for (int i = 0; i < product_id.Length; i++)
        {

            if (i == 0)
            {
                Genarate_static(i, Shop_panel_script.Instance.first);
            }
            else if (i > 0 && i < 3)
            {
                Genarate_static(i, Shop_panel_script.Instance.combo1.transform.GetChild(i - 1).gameObject);
                Shop_panel_script.Instance.combo1.transform.GetChild(i - 1).gameObject.SetActive(true);
            }
            else if (i == 3)
            {
                Genarate_static(i, Shop_panel_script.Instance.forth);
                Shop_panel_script.Instance.forth.SetActive(true);
            }
            else if (i > 3)
            {
                if (i % 2 == 0)
                {
                    temp_combo = Instantiate(Shop_panel_script.Instance.combo1);
                    temp_combo.transform.SetParent(Shop_panel_script.Instance.Scroll_content.transform);
                    temp_combo.transform.localScale = Vector3.one;
                }
                Genarate_static(i, temp_combo.transform.GetChild(i % 2).gameObject);
                temp_combo.transform.GetChild(i % 2).gameObject.SetActive(true);
            }
            //Debug.Log(i);
        }
       
    }

    private void Genarate(int i, GameObject jkl)
    {
        GameObject ab = Instantiate(jkl, Vector3.zero, Quaternion.identity);
        ab.transform.SetParent(Shop_panel_script.Instance.Scroll_content.transform);
        ab.transform.localScale = new Vector3(1, 1, 1);
        ab.GetComponent<IAP_Buttons>().Coin_value.text = first_val[i].ToString();
        ab.GetComponent<IAP_Buttons>().Discount_value.text = percentage_val[i].ToString() + "%";
        ab.GetComponent<IAP_Buttons>().Buy_value.text = price_val[i];
        //ab.GetComponent<IAP_Buttons>().Final_value.text = (first_val[i] + ((first_val[i] * percentage_val[i]) / 100)).ToString();
        ab.GetComponent<IAP_Buttons>().number = i;
        //ab.GetComponent<Purchase_container_script>().buy_button.GetComponent<Button>().onClick.AddListener(() => GetComponent<InAppManager>().BuyProductID(Get_API_Data_IAP.Instance.product_id[i]));
        ab.GetComponent<IAP_Buttons>().ONCLICK_but();
    }
    private void Genarate_static(int i, GameObject jkl)
    {
        //GameObject ab = Instantiate(jkl, Vector3.zero, Quaternion.identity);
        //ab.transform.SetParent(Shop_panel_script.Instance.Scroll_content.transform);
        //ab.transform.localScale = new Vector3(1, 1, 1);
        jkl.GetComponent<IAP_Buttons>().Coin_value.text = first_val[i].ToString()+ " Coins";
        if (jkl.GetComponent<IAP_Buttons>().Discount_value != null)
        {
            jkl.GetComponent<IAP_Buttons>().Discount_value.text = percentage_val[i].ToString() + "%";
        }
        jkl.GetComponent<IAP_Buttons>().Buy_value.text = price_val[i];
        //ab.GetComponent<IAP_Buttons>().Final_value.text = (first_val[i] + ((first_val[i] * percentage_val[i]) / 100)).ToString();
        jkl.GetComponent<IAP_Buttons>().number = i;
        //ab.GetComponent<Purchase_container_script>().buy_button.GetComponent<Button>().onClick.AddListener(() => GetComponent<InAppManager>().BuyProductID(Get_API_Data_IAP.Instance.product_id[i]));
        jkl.GetComponent<IAP_Buttons>().ONCLICK_but();
    }

    public void on_startup_purchase_card()
    {
        //Debug.Log("here");
        //int ax = Get_API_Data_IAP.Instance.first_val.Length - 1;
        //startup_purchase_card_panel.GetComponent<Purchase_container_script>().Main_value.text = St_purchase_first_val.ToString();
        //startup_purchase_card_panel.GetComponent<Purchase_container_script>().Percentage_value.text = St_purchase_percentage_val.ToString() + "%\nOff";
        //startup_purchase_card_panel.GetComponent<Purchase_container_script>().Buy_value.text = St_purchase_price_val;
        //startup_purchase_card_panel.GetComponent<Purchase_container_script>().Final_value.text = (St_purchase_first_val + ((St_purchase_first_val * St_purchase_percentage_val) / 100)).ToString();
        //startup_purchase_card_panel.GetComponent<Purchase_container_script>().number = -1;
        //startup_purchase_card_panel.GetComponent<Purchase_container_script>().ONCLICK_but();

    }


    //public void turnon_startup_purchase_panel()
    //{
    //    if (home_panel.activeSelf)
    //    {
    //        startup_purchase_card_panel.SetActive(true);
    //    }
    //}

    //public void rep_startup_purchasecard()
    //{
    //    InvokeRepeating("startup_purchase_check", 0f, 0.5f);
    //}



}

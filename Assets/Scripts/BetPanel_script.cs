﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BetPanel_script : MonoBehaviour
{

    public static BetPanel_script I;


    public GameObject bet_panel;
    public Text Get_value, Price, User_value, have_value; 
    
    
    private void Awake()
    {

        I = this;
    }


    public void Start_betPanel(int fee)
    {
        if(GameManager.Instance.Check_coins(fee))
        {
            GameManager.Instance.fee = fee;
            GameManager.Instance.reward = fee * 3;

            Price.text = GameManager.Instance.reward.ToString();
            Get_value.text = GameManager.Instance.reward.ToString();
            User_value.text = GameManager.Instance.Coin_var.ToString();
            have_value.text = (GameManager.Instance.reward + GameManager.Instance.Coin_var).ToString();
            bet_panel.SetActive(true);

        }
        else
        {
            GameManager.Instance.nem_popup_fun("Not Enough Coins...");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

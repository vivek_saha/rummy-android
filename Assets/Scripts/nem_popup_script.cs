﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
public class nem_popup_script : MonoBehaviour
{
    public TextMeshProUGUI msg;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;
        Move_up();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Move_up()
    {
        transform.DOLocalMoveY(transform.localPosition.y + (Screen.height/2), 1f).OnComplete(() => { Destroy(this.gameObject); }).SetUpdate(true);
    }

    
}

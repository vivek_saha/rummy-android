﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityNative.Toasts.Example;

public class edit_profile_panel : MonoBehaviour
{

    public static edit_profile_panel I;


    public GameObject BG;
    public InputField name_field;
    public Image Profile_pic;
    string profile_pic_link_1 = "http://104.131.36.193:6800/";
    public string profile_pic_link_2;
    public Text validation_msg;

    //public Image Profile_pic;
    public string filename;
    public string extension;

    //[HideInInspector]
    public Sprite[] unselected, selected, for_profilepic;

    [HideInInspector]
    public Button[] allprofiles;
    //guestscript
    public Texture2D temp_tex;
    [HideInInspector]
    public byte[] itemBGBytes;

    public int selected_profile
    {
        get
        {
            return PlayerPrefs.GetInt("selected_profile", 0);
        }
        set
        {
            PlayerPrefs.SetInt("selected_profile", value);
            reset_selected_profile();
        }
    }

    private void Awake()
    {
        I = this;
    }


    // Start is called before the first frame update
    void Start()
    {
        selected_profile = selected_profile;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Get_image()
    {
        StartCoroutine("downloadImg", profile_pic_link_1 + profile_pic_link_2);
    }

    IEnumerator downloadImg(string url)
    {
        Texture2D texture = new Texture2D(1, 1);
        WWW www = new WWW(url);
        yield return www;
        www.LoadImageIntoTexture(texture);

        Sprite image = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        Profile_pic.sprite = image;
        Homepanel_script.I.set_profile_pic(image);
        //Homepanel_script.I.profile_pic.sprite = image;
        //Homepanel_script.I.profile_pic_profile_panel.sprite = image;
    }
    public void On_click_Save()
    {
        if (Input_Check())
        {
            Save_image_to_file(Profile_pic.sprite.texture);
            //Homepanel_script.I.profile_pic.sprite = Profile_pic.sprite;
            //Homepanel_script.I.profile_pic_profile_panel.sprite = Profile_pic.sprite;
            //Homepanel_script.I.u_name.text = name_field.text;
            //Homepanel_script.I.u_name_profile_panel.text = name_field.text;
            //if(Setting_API.Instance!= null)
            //Debug.Log(Profile_pic.sprite.name);

            //Setting_API.Instance.profile_piiiiccccccc.sprite = Profile_pic.sprite;
            if (PlayerPrefs.GetString("Token", string.Empty) == string.Empty)
            {
                //login
                guest_login.I.U_name = name_field.text;
                guest_login.I.Onlogin_guest();
                BG.SetActive(false);
            }
            else
            {
                //update
                BG.SetActive(false);
                Update_API.Instance.is_profile = 1;
                Update_API.Instance.username = name_field.text; ;
                Update_API.Instance.Update_data();
            }
        }
    }


    #region Validation
    public bool Input_Check()
    {
        if (name_field.text == string.Empty)
        {
            //Debug.Log("empty");
            //validation_msg.text = "Username Can't be Empty.";
            Native_Toast("Username Can't be Empty.");
            //StartCoroutine("hide_validation_msg");
            return false;
        }
        else if (name_field.text.Length < 4)
        {
            //Debug.Log("size");
            //validation_msg.text = "Username Should Contain Atleast 4 Characters";
            Native_Toast("Username Should Contain Atleast 4 Characters");
            //StartCoroutine("hide_validation_msg");
            return false;
        }
        else
        {
            return true;
        }
    }

    public void Native_Toast(string msg)
    {
        UnityNativeToastsHelper.ShowLongToast(msg);
    }

    IEnumerator hide_validation_msg()
    {
        yield return new WaitForSeconds(2f);
        validation_msg.text = string.Empty;
    }
    #endregion

    public void on_click_profile(int i)
    {
        selected_profile = i;
    }
    public void reset_selected_profile()
    {

        for (int i = 0; i < allprofiles.Length; i++)
        {
            if (i == selected_profile)
            {
                allprofiles[i].GetComponent<Image>().sprite = selected[i];
                Profile_pic.sprite = for_profilepic[i];
            }
            else
            {
                allprofiles[i].GetComponent<Image>().sprite = unselected[i];
            }
        }
    }

    public void Ongallery_click()
    {
        PickImage(512);
    }
    private void PickImage(int maxSize)
    {
        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {

            filename = Path.GetFileName(path);

            string a = Path.GetExtension(path);
            a = a.Replace(".", string.Empty);
            extension = a;
            if (path != null)
            {
                // Create Texture from selected image
                Texture2D texture = NativeGallery.LoadImageAtPath(path, maxSize);
                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    return;
                }
                texture = duplicateTexture(texture);
                texture = RoundCrop(texture);
                Profile_pic.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                Profile_pic.sprite.name = filename;
                selected_profile = -1;
                //Save_image_to_file(texture);
                //Guest_login_script.Instance.Save_image_to_file(texture);
            }
        }, "Select a PNG image", "image/png");

        //Debug.Log("Permission result: " + permission);
    }

    public void Set_profile_pic()
    {
        string ax = Application.persistentDataPath + "/profile_pic/temp.png";
        Homepanel_script.I.set_profile_pic(LoadSprite(ax));
    }


    public void Save_image_to_file(Texture2D tex)
    {
        tex = duplicateTexture(tex);
        itemBGBytes = tex.EncodeToPNG();
        FileChk();
        File.WriteAllBytes(Application.persistentDataPath + "/profile_pic/temp.png", itemBGBytes);
    }

    private Sprite LoadSprite(string path)
    {
        if (string.IsNullOrEmpty(path)) return null;
        if (System.IO.File.Exists(path))
        {
            byte[] bytes = File.ReadAllBytes(path);
            Texture2D texture = new Texture2D(900, 900, TextureFormat.RGB24, false);
            texture.filterMode = FilterMode.Trilinear;
            texture.LoadImage(bytes);
            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, 8, 8), new Vector2(0.5f, 0.0f), 1.0f);

            // You should return the sprite here!
            return sprite;
        }
        return null;
    }

    public void FileChk()
    {
        if (!Directory.Exists(Application.persistentDataPath + "/profile_pic"))
        {
            //if it doesn't, create it
            Directory.CreateDirectory(Application.persistentDataPath + "/profile_pic");

        }
        string filePath = Application.persistentDataPath + "/profile_pic/temp.png";

        if (System.IO.File.Exists(filePath))
        {
            // The file exists -> run event
            File.Delete(filePath);
        }
        else
        {
            // The file does not exist -> run event
        }
    }

    #region Texture changes
    Texture2D duplicateTexture(Texture2D source)
    {
        RenderTexture renderTex = RenderTexture.GetTemporary(
                    source.width,
                    source.height,
                    0,
                    RenderTextureFormat.Default,
                    RenderTextureReadWrite.Linear);

        Graphics.Blit(source, renderTex);
        RenderTexture previous = RenderTexture.active;
        RenderTexture.active = renderTex;
        Texture2D readableText = new Texture2D(source.width, source.height);
        readableText.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
        readableText.Apply();
        RenderTexture.active = previous;
        RenderTexture.ReleaseTemporary(renderTex);
        return readableText;
    }

    Texture2D RoundCrop(Texture2D sourceTexture)
    {
        int width = sourceTexture.width;
        int height = sourceTexture.height;
        float radius = (width < height) ? (width / 2f) : (height / 2f);
        float centerX = width / 2f;
        float centerY = height / 2f;
        Vector2 centerVector = new Vector2(centerX, centerY);

        // pixels are laid out left to right, bottom to top (i.e. row after row)
        Color[] colorArray = sourceTexture.GetPixels(0, 0, width, height);
        Color[] croppedColorArray = new Color[width * height];
        //Texture2D croppedTexture_change = new Texture2D((int)radius, (int)radius);
        for (int row = 0; row < height; row++)
        {
            for (int column = 0; column < width; column++)
            {
                int colorIndex = (row * width) + column;
                float pointDistance = Vector2.Distance(new Vector2(column, row), centerVector);
                if (pointDistance < radius)
                {
                    croppedColorArray[colorIndex] = colorArray[colorIndex];
                    //croppedTexture_change.SetPixel(column,row, colorArray[colorIndex]);
                }
                else
                {
                    //sourceTexture.SetPixel(row, column,);
                    //croppedColorArray[colorIndex] = ;
                    croppedColorArray[colorIndex] = Color.clear;
                }
            }
        }

        Texture2D croppedTexture = new Texture2D(width, height);
        croppedTexture.SetPixels(croppedColorArray);
        croppedTexture.Apply();
        //croppedTexture_change.Apply();
        croppedTexture = TextureTools.ResampleAndCrop(croppedTexture, (int)radius, (int)radius);
        croppedTexture.Apply();
        return croppedTexture;
    }
    #endregion

}
public class TextureTools
{
    public static Texture2D ResampleAndCrop(Texture2D source, int targetWidth, int targetHeight)
    {
        int sourceWidth = source.width;
        int sourceHeight = source.height;
        float sourceAspect = (float)sourceWidth / sourceHeight;
        float targetAspect = (float)targetWidth / targetHeight;
        int xOffset = 0;
        int yOffset = 0;
        float factor = 1;
        if (sourceAspect > targetAspect)
        { // crop width
            factor = (float)targetHeight / sourceHeight;
            xOffset = (int)((sourceWidth - sourceHeight * targetAspect) * 0.5f);
        }
        else
        { // crop height
            factor = (float)targetWidth / sourceWidth;
            yOffset = (int)((sourceHeight - sourceWidth / targetAspect) * 0.5f);
        }
        Color32[] data = source.GetPixels32();
        Color32[] data2 = new Color32[targetWidth * targetHeight];
        for (int y = 0; y < targetHeight; y++)
        {
            for (int x = 0; x < targetWidth; x++)
            {
                var p = new Vector2(Mathf.Clamp(xOffset + x / factor, 0, sourceWidth - 1), Mathf.Clamp(yOffset + y / factor, 0, sourceHeight - 1));
                // bilinear filtering
                var c11 = data[Mathf.FloorToInt(p.x) + sourceWidth * (Mathf.FloorToInt(p.y))];
                var c12 = data[Mathf.FloorToInt(p.x) + sourceWidth * (Mathf.CeilToInt(p.y))];
                var c21 = data[Mathf.CeilToInt(p.x) + sourceWidth * (Mathf.FloorToInt(p.y))];
                var c22 = data[Mathf.CeilToInt(p.x) + sourceWidth * (Mathf.CeilToInt(p.y))];
                var f = new Vector2(Mathf.Repeat(p.x, 1f), Mathf.Repeat(p.y, 1f));
                data2[x + y * targetWidth] = Color.Lerp(Color.Lerp(c11, c12, p.y), Color.Lerp(c21, c22, p.y), p.x);
            }
        }

        var tex = new Texture2D(targetWidth, targetHeight);
        tex.SetPixels32(data2);
        tex.Apply(true);
        return tex;
    }
}

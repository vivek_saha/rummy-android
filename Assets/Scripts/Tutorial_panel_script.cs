﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Tutorial_panel_script : MonoBehaviour
{

    public static Tutorial_panel_script I;

    public Image img;
    public Sprite[] panels;
    public int Counter = 0;

    private void Awake()
    {
        I = this;
    }

    public void onclick()
    {
        if (Counter == 17)
        {
            Counter = 0;
            if(GameManager.Instance.Level_var==0)
            {
                GameManager.Instance.Level_var += 1;
                GameManager.Instance.set_lvlup_panel();

            }
            gameObject.SetActive(false);
        }
        else
        {


            Counter++;
            img.sprite = panels[Counter];
        }
    }



    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}

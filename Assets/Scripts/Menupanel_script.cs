﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using rummy;

public class Menupanel_script : MonoBehaviour
{

    public static Menupanel_script I;

    [HideInInspector]
    public Toggle sound, vibrate;
    [HideInInspector]
    public GameObject sound_off, sound_on, vibrate_off, vibrate_on;
    public GameObject game_master; 

    public GameObject facebook_obj;
    private void Awake()
    {
        I = this;
    }

    private void OnEnable()
    {
        Time.timeScale = 0;
        game_master.GetComponent<GameMaster>().TogglePause();
        setToggles();
    }

    public void setToggles()
    {
        sound_on.SetActive(Audiomanager.Instance.Sound_bool);
        sound_off.SetActive(!Audiomanager.Instance.Sound_bool);
       
        vibrate_on.SetActive(Audiomanager.Instance.Vibrate_bool);
        vibrate_off.SetActive(!Audiomanager.Instance.Vibrate_bool);
        sound.isOn = Audiomanager.Instance.Sound_bool;
        
        vibrate.isOn = Audiomanager.Instance.Vibrate_bool;
    }


    public void toogle_sound()
    {
        //Debug.Log(sound.isOn);
        Audiomanager.Instance.Sound_bool = sound.isOn;
        sound_on.SetActive(Audiomanager.Instance.Sound_bool);
        sound_off.SetActive(!Audiomanager.Instance.Sound_bool);
    }



    public void toogle_vibrate()
    {
        //Debug.Log(vibrate.isOn);
        Audiomanager.Instance.Vibrate_bool = vibrate.isOn;
        vibrate_on.SetActive(Audiomanager.Instance.Vibrate_bool);
        vibrate_off.SetActive(!Audiomanager.Instance.Vibrate_bool);
    }


    public void Close_menu_panel()
    {
        Time.timeScale = 1;
        game_master.GetComponent<GameMaster>().TogglePause();
        gameObject.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

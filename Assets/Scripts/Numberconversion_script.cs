﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Numberconversion_script : MonoBehaviour
{

    public static Numberconversion_script Instance;

    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        //Some tests
        //Debug.Log("123000 is " + AbbrevationUtility.AbbreviateNumber(123000));
        //Debug.Log("456000000 is " + AbbrevationUtility.AbbreviateNumber(456000000));
        //Debug.Log("789000000000 is " + AbbrevationUtility.AbbreviateNumber(789000000000));
        //Debug.Log("1000 is " + AbbrevationUtility.AbbreviateNumber(1000));
        //Debug.Log("1150000 is " + AbbrevationUtility.AbbreviateNumber(1150000));
        //Debug.Log("135000000000000 is " + AbbrevationUtility.AbbreviateNumber(135000000000000));
    }

    public string Numbercon(long a)
    {
        return AbbrevationUtility.AbbreviateNumber(a);
    }
}

public static class AbbrevationUtility
{
    private static readonly SortedDictionary<long, string> abbrevations = new SortedDictionary<long, string>
     {
         //{1000,"K"},
         {1000000, "M" },
         {1000000000, "B" },
         {1000000000000,"T"}
     };

    public static string AbbreviateNumber(float number)
    {
        for (int i = abbrevations.Count - 1; i >= 0; i--)
        {    
            KeyValuePair<long, string> pair = abbrevations.ElementAt(i);
            if (Mathf.Abs(number) >= pair.Key)
             {
            float roundedNumber = (number / pair.Key);
                roundedNumber = (float)System.Math.Round(roundedNumber, 2);
            return roundedNumber.ToString() + pair.Value;
        }
    }
         return number.ToString();
     }
 }
public static class AbbrevationUtility_LeaderBoard
{
    private static readonly SortedDictionary<long, string> abbrevations = new SortedDictionary<long, string>
     {
         {1000,"K"},
         {1000000, "M" },
         {1000000000, "B" },
         {1000000000000,"T"}
     };

    public static string AbbreviateNumber(float number)
    {
        for (int i = abbrevations.Count - 1; i >= 0; i--)
        {
            KeyValuePair<long, string> pair = abbrevations.ElementAt(i);
            if (Mathf.Abs(number) >= pair.Key)
            {
                float roundedNumber = (number / pair.Key);
                roundedNumber = (float)System.Math.Round(roundedNumber, 2);
                return roundedNumber.ToString() + pair.Value;
            }
        }
        return number.ToString();
    }
}
﻿using rummy.Cards;
using rummy.Utility;
using UnityEngine;

public class Drag_drop_script : MonoBehaviour
{
    public static Drag_drop_script I;
    private Ray ray;
    private RaycastHit hit;
    public Color clr;
    private GameObject drag_obj;
    public bool isdragging = false;
    //public Transform Preparent_transform;

    private void Awake()
    {
        I = this;
    }

    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            // hit =;
            Debug.DrawRay(ray.origin, ray.direction*1000f, Color.green);
            if (Physics.Raycast(ray.origin, ray.direction, out hit, Mathf.Infinity))
            {
                //print(hit.collider.name);
                if (hit.collider.GetComponent<Card>() != null)
                {
                    //drag_obj = null;
                    if (hit.collider.transform.parent.transform.GetComponent<GameBoardTile>().filled)

                    {
                        drag_obj = hit.collider.gameObject;
                        isdragging = true;
                        hit.collider.transform.parent.transform.GetComponent<GameBoardTile>().filled = false;
                        Vector3 tt = hit.collider.transform.parent.transform.position;
                        tt.z = -1;

                        Debug.Log(hit.collider.transform.parent.transform.parent.gameObject.name);
                        string nname = hit.collider.transform.parent.transform.parent.gameObject.name;
                        if (nname == "HandCardSpotUP" || nname == "HandCardSpotDown")
                        {
                            Tb.I.hdcspot.RemoveCard(hit.collider.GetComponent<Card>());
                        }
                        set_initial_size_card(hit.collider.GetComponent<Card>());
                        drag_obj.GetComponent<Card>().set_prepos(tt);
                        drag_obj.transform.SetParent(gameObject.transform);
                    }
                }
                else if (hit.collider.transform.childCount > 0)
                {
                    if (hit.collider.transform.GetChild(0).GetComponent<Card>() != null)
                    {
                        if (hit.collider.transform.GetComponent<GameBoardTile>().filled)
                        {
                            if (hit.collider.transform.GetComponent<GameBoardTile>().Gameboard_card)
                            {
                                isdragging = true;
                                drag_obj = hit.collider.gameObject.transform.GetChild(0).gameObject;
                                hit.collider.transform.GetComponent<GameBoardTile>().filled = false;
                                Vector3 tt = hit.collider.transform.position;
                                tt.z = -1;
                                drag_obj.GetComponent<Card>().set_prepos(tt);
                                drag_obj.transform.SetParent(gameObject.transform);
                                drag_obj.SetActive(true);
                                hit.collider.GetComponent<GameBoardTile>().OnMouseExit();
                            }
                        }
                    }
                }

                //Debug.Log("nothing hit");
            }
        }
        if (isdragging)
        {
            //drag_obj.GetComponent<SpriteRenderer>().color = Color.red;
            Vector3 temmp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //GetComponent<BoxCollider>().enabled = false;
            temmp.z = -1;
            //transform.position = temmp;
            drag_obj.transform.position = temmp;
        }
        else
        {
            if (drag_obj != null)
            {
                drag_obj.GetComponent<Card>().normal_fun();
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            isdragging = false;
            if (drag_obj != null)
            {
                drag_obj.GetComponent<Card>().backer = true;
                drag_obj.GetComponent<Card>().isdragging = false;
                drag_obj.GetComponent<Card>().normal_fun();
                drag_obj = null;
            }
        }
    }

    public void set_initial_size_card(Card cd)
    {
        cd.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
    }
}
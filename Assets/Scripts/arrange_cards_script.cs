﻿using rummy.Cards;
using rummy.Utility;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class arrange_cards_script : MonoBehaviour
{

    public static arrange_cards_script I;
    public int Set_value = 0;
    public HandCardSpot hdcspot;
    public TextMeshPro value;
    public SpriteRenderer sp;
    //private CardCombo laydownCards = new CardCombo();
    [SerializeField]
    private List<Card> laydownCards = new List<Card>();

    [SerializeField]
    private List<Card> Showcards = new List<Card>();

    private List<string> adc;
    public CardCombo cdcombo;






    private void Awake()
    {
        I = this;
    }






    // Start is called before the first frame update
    private void Start()
    {
        value.text = Set_value.ToString();
    }


    //  Get Possible sets

    public void Arrange_cards()
    {
        //List<Card> ac = new List<Card>();
        laydownCards.Clear();
        Showcards.Clear();
        List<Card> ac = hdcspot.Objects;
        ////ac.RemoveAll(null);
        //foreach(Card  a in ac)
        //{
        //    if(a==null)
        //    {
        //        ac.Remove(a);
        //    }
        //    //ac.Remove()
        //}

        for (int i = 0; i < ac.Count; i++)
        {
            if (ac[i] == null)
            {
                ac.Remove(ac[i]);
            }
        }
        var combo = CardUtil.GetAllUniqueCombos(ac, laydownCards);
        //cdcombo = combo;
        foreach (var a in combo)
        {
            Debug.Log(a.Value);

            Set_value = a.Value;
            value.text = Set_value.ToString();
            //cdcombo.
            //Debug.Log(a);
            //a.Sets
        }
        foreach (CardCombo a in combo)
        {
            //Debug.Log( a.);
            laydownCards = a.GetCards();
            Showcards = a.GetCards();
            Showcards.Add(null);
        }

        //Debug.Log(laydownCards);
        //laydownCards = combo;

        // Get Possible duo sets
        var duuo = CardUtil.GetAllDuoSets(ac, laydownCards, ref adc);
        foreach (var a in duuo)
        {
            bool existed = false;
            foreach (Card s in laydownCards)
            {
                if (a.A == s || a.B == s)
                {
                    existed = true;
                    break;
                }
                else
                {
                }
            }
            if (existed)
            {
                //duuo.Remove(a);
            }
            else
            {
                laydownCards.Add(a.A);
                laydownCards.Add(a.B);
                Showcards.Add(a.A);
                Showcards.Add(a.B);
                Showcards.Add(null);
            }
            //Debug.Log(a);
        }
        //foreach(var a in duuo)
        //{
        //    Debug.Log(a);
        //}
        var duuoR = CardUtil.GetAllDuoRuns(ac, laydownCards, ref adc);
        foreach (var a in duuoR)
        {
            bool existed = false;
            foreach (Card s in laydownCards)
            {
                if (a.A == s || a.B == s)
                {
                    existed = true;
                    break;
                }
                else
                {
                }
            }
            if (existed)
            {
                //duuoR.Remove(a);
            }
            else
            {
                laydownCards.Add(a.A);
                laydownCards.Add(a.B);
                Showcards.Add(a.A);
                Showcards.Add(a.B);
                Showcards.Add(null);
            }

            //Debug.Log(a);
        }
        //hdcspot.ResetSpot();
        foreach (Card a in laydownCards)
        {
            //Debug.Log(a);
            hdcspot.RemoveCard(a);
        }

        //adding remaining cards
        int pairedcard = Showcards.Count;
        foreach (Card x in hdcspot.Objects)
        {
            laydownCards.Add(x);
            Showcards.Add(x);
        }

        //remove remaining cards
        foreach (Card a in laydownCards)
        {
            //Debug.Log(a);
            //Card m = hdcspot.Objects.Find(x => x == a);
            //Debug.Log("m" + m);
            if (hdcspot.Objects.Contains(a))
            {
                hdcspot.RemoveCard(a);
            }
        }

        hdcspot.ResetSpot();

        //for (int i = 36 - hdcspot.Objects.Count;i<36;i++)
        //{
        //    Showcards.Insert(i, hdcspot.Objects[i- teempp]);
        //}

        //foreach (Card a in laydownCards)
        //{
        //    Debug.Log(a);
        //}

        //add cards
        //foreach (Card a in Showcards)
        //{
        //    hdcspot.AddCard(a);
        //}
        for (int i = 0; i < Showcards.Count; i++)
        {
            //if(i< pairedcard)
            {
                hdcspot.draw_card_bool = true;
                hdcspot.AddCard(Showcards[i]);
            }
            //else
            //{
            //    //Debug.Log(hdcspot.Objects.GetRange());
            //    Debug.Log(36 - (Showcards.Count - i));
            //    hdcspot.AddCard(Showcards[i],36-(Showcards.Count-i));
            //}
        }

        //Get Possible Joker sets
    }

    public void auto_meld_cards()
    {
        if (Showcards.Count > 0)
        {
            if (GameBoard.Instance.player_obj.Initial_meld)
            {
                
                if (Set_value >= 30)
                {
                    foreach (Card a in return_combo_cards(Showcards))
                    {
                        Vector3 teeeeemp = a.transform.parent.transform.position;
                        teeeeemp.z = -1;
                        UndoRedoSystem.I.TrackChangeAction(UndoableActionType.automeld, a.gameObject, teeeeemp, a.transform.position);
                        Gamplay_panel_script.I.set_undo_but(true);
                        hdcspot.RemoveCard(a);
                    }
                    //get_(return_combo_cards(Showcards));
                    GameBoard.Instance.Board_card_add(return_combo_cards(Showcards));
                    GameBoard.Instance.Automeld_bool = true;
                    Showcards.Clear();
                    //Arrange_cards();
                }
                else
                {
                    string ax = "Initial meld must equal 30 points or more.";
                    Debug.Log("Initial meld must equal 30 points or more.");
                    Gamplay_panel_script.I.nem_popup_fun(ax);
                }
            }
            else
            {
                foreach (Card a in return_combo_cards(Showcards))
                {
                    Vector3 teeeeemp = a.transform.parent.transform.position;
                    teeeeemp.z = -1;
                    UndoRedoSystem.I.TrackChangeAction(UndoableActionType.automeld, a.gameObject, teeeeemp, a.transform.position);
                    Gamplay_panel_script.I.set_undo_but(true);
                    hdcspot.RemoveCard(a);
                }
                //get_(return_combo_cards(Showcards));
                GameBoard.Instance.Board_card_add(return_combo_cards(Showcards));
                GameBoard.Instance.Automeld_bool = true;
                Showcards.Clear();
            }
        }
    }

    private static void get_(List<Card> cl)
    {
        var combo = CardUtil.GetAllUniqueCombos(cl, null);
        CardCombo cadcom = new CardCombo();
        //log for combo
        //Debug.Log(combo.ToString());
        foreach (CardCombo a in combo)
        {
            Debug.Log(a.ToString());
            cadcom = a;
            //Debug.Log("sets"+ a.Sets.Count);
            //Debug.Log("runs" + a.Runs.Count);
        }

        List<int> counter_cards_combos = new List<int>();
        if (cadcom.Sets.Count > 0)
        {
            foreach (Set s in cadcom.Sets)
            {
                counter_cards_combos.Add(s.Count);
            }
        }
        if (cadcom.Runs.Count > 0)
        {
            foreach (Run r in cadcom.Runs)
            {
                counter_cards_combos.Add(r.Count);
            }
        }

        //space counter 
        int counter_space = 0;
        //counter_cards_combos
        int ccc_counter = 0;

        //for()

        // main list card counter
        int cardcounter = 0;
    }

    public List<Card> return_combo_cards(List<Card> cs)
    {
        List<Card> rr = new List<Card>();
        foreach (Card a in cs)
        {
            if (a == null)
            {
                return rr;
            }
            else
            {
                rr.Add(a);
            }
        }
        return null;
    }

    //  Prioritize sets according to the values
    private int CalculateValue(List<Card> Cards)
    {
        int value = 0;
        int Count = Cards.Count;
        for (int i = 0; i < Count; i++)
        {
            var rank = Cards[i].Rank;
            if (rank == Card.CardRank.JOKER)
            {
                if (i == 0 && Cards.Count == 1)
                {
                    // The joker is the first and only card in the run,
                    // which will have value zero for now
                    break;
                }

                var jokerRank = CardUtil.GetJokerRank(Cards, i);

                // If the next card is a TWO, jokerRank is RANK(2-1)=Rank(1)=JOKER
                if (jokerRank == Card.CardRank.JOKER)
                {
                    // JOKER is ACE, ACE counts 1 in ACE-2-3
                    value += 1;
                }
                else
                {
                    value += Card.CardValues[jokerRank];
                }
            }
            else if (rank == Card.CardRank.ACE && i == 0)
            {
                // ACE counts 1 in ACE-2-3
                value += 1;
            }
            else
            {
                value += Cards[i].Value;
            }
        }
        return value;
        //Tb.I.GameMaster.debug_value(Value);
    }

    // Update is called once per frame
    private void Update()
    {
    }
}
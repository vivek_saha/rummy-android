﻿using UnityEngine;

public class GameBoardTile : MonoBehaviour
{
    public int x;
    public int y;
    public bool filled = false;
    private Color abc = Color.black;
    private Color highlight_color = Color.green;
    public Sprite default_sprite;

    public bool Gameboard_card = true;

    // Start is called before the first frame update
    private void Start()
    {
        if (Gameboard_card)
        {
            //Debug.Log((float)x - 0.5f);
            default_sprite = GetComponent<SpriteRenderer>().sprite;
            this.transform.position = (Vector3.right * x * 2f) + (Vector3.up * y * 2.6f);
        }
    }

    public void OnMouseExit()
    {
        if (!filled)
        {
            if (Gameboard_card)
            {
                GetComponent<SpriteRenderer>().sprite = default_sprite;

                //abc.a = 0.6470588f;
                //GetComponent<SpriteRenderer>().color = abc;
            }
        }
    }

    public void OnMouseOver()
    {
        //highlight_color.a = 0.6470588f;
        if (Drag_drop_script.I.isdragging == true)
        {
            if (!filled)
            {
                if (Gameboard_card)
                {
                    //GetComponent<SpriteRenderer>().color = highlight_color;
                }
            }
        }
    }

    private void OnMouseDown()
    {
        //if (filled)
        //{
        //    if (transform.childCount > 0)
        //    {
        //        transform.GetChild(0).gameObject.SetActive(true);
        //        transform.GetChild(0).gameObject.GetComponent<BoxCollider>().enabled = true;
        //        transform.GetChild(0).gameObject.GetComponent<Card>().isdragging = true;
        //        //transform.GetChild(0).gameObject.GetComponent<Card>().
        //        Debug.Log(transform.position);
        //        Vector3 teeeeeemp = transform.position;
        //        teeeeeemp.z = -1;
        //        transform.GetChild(0).gameObject.GetComponent<Card>().pre_pos = teeeeeemp;
        //        filled = false;
        //        OnMouseExit();
        //    }
        //}
    }

    private void OnMouseUp()
    {
    }

    //public void release_adder_card(GameObject a)
    //{
    //    GetComponent<SpriteRenderer>().sprite= a.GetComponent<SpriteRenderer>().sprite;
    //    GetComponent<SpriteRenderer>().color = Color.white;
    //    a.SetActive(false);
    //}

    //private void OnMouseUp()
    //{
    //    if (filled)
    //    {
    //        filled = false;
    //        OnMouseExit();
    //    }
    //}
    //private void OnMouseUp()
    //{
    //    GetComponent<SpriteRenderer>().color = Color.red;
    //}
}